import * as React from 'react';
import { StyleSheet } from 'react-native';
import Navigation from '../navigation';
import useColorScheme from '../hooks/useColorScheme';
import LoginScreen from './LoginScreen';
import { UsersContext } from '../context/Users/context';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MainRoot } from '../types';
import RegisterScreen from './RegisterScreen';

export default function MainScreen(props:any) {
  const user = React.useContext(UsersContext)
  const colorScheme = useColorScheme();
  const Stack = createStackNavigator<MainRoot>();

  return (
    <>
    {
      user.state.token ? (
        <Navigation colorScheme={colorScheme} />
      ) : (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
              <Stack.Screen name="Login" component={LoginScreen} />
              <Stack.Screen name="Register" component={RegisterScreen} />
            </Stack.Navigator>
        </NavigationContainer>
      )
    }
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo:{ 
    width: 200,
    height:200,
    resizeMode:'contain',
    marginTop:30
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input:{
    flex:1,
    marginHorizontal:20,
    marginVertical:10
  },
  button: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#01573c',
    color: '#fff'
  },
});
