import * as React from 'react';
import { StyleSheet, Image } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Text, Input, Layout, Icon, Divider, ListItem, Button, Card } from '@ui-kitten/components';
import { View } from '../../components/Themed';
import { PageNavigation } from '../../components/TopNavigation';
import { UserContext } from '../../context/Auth/context';
import NotFoundScreen from '../NotFoundScreen';
import { ScrollView } from 'react-native-gesture-handler';


const CalendarIcon = (props) => (
  <Icon {...props} name='calendar' />
);

const data = new Array(8).fill({
  title: 'Nama Customer',
  description: 'Tanggal Transaksi',
});

export default function DetailRekap(navigation: any) {
  const user = React.useContext(UserContext)
  const [date, setDate] = React.useState(new Date())

  const renderItemAccessory = (props) => (
    <Button size='medium' onPress={() => navigation.navigation.navigate('DetailRekap', {
      tabBarVisibility: false,
    })}>Detail</Button>
  );

  const renderItem = ({ item, index }) => (
    <ListItem
      title={`${item.title} ${index + 1}`}
      description={`${item.description} ${index + 1}`}

      accessoryRight={renderItemAccessory}
    />
  );

  // Card 
  const Header = (props) => (
    <View {...props}>

      <View style={{ flexDirection: 'row', marginBottom: 14 }}>
        <View style={{ width: 60 }}>
          <Image source={require('../../assets/icon/garansi.png')} style={styles.logo} />
        </View>
        <View style={{ width: 150 }}>
          <Button style={styles.button} appearance='outline' status='success'>
            Garansi 1 Bulan
        </Button>
        </View>
      </View>

      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 1 }}>
          <Text category='h6'>Iphone 11 256 Black</Text>
        </View>
        <View style={{ flex: 0.5 }}>
          <Text category=''>Qty: 1</Text>
        </View>
      </View>


    </View>
  );

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Detail Order" ></PageNavigation>
      <ScrollView style={{ marginBottom: 40 }}>

        <View style={styles.rowContainer}>
          <Text category="h5" style={{ flex: 1, textAlign: 'left' }}>Pesanan</Text>
        </View>
        <View style={styles.rowContainer}>
          <React.Fragment>
            <Layout style={styles.topContainer} level='1'>
              <Card style={styles.card} header={Header}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1 }}>
                    <Text>IMEI 12345</Text>
                  </View>
                  <View style={{ flex: 0.5 }}>
                    <Text category=''>IDR 10,800,000</Text>
                  </View>
                </View>
              </Card>
            </Layout>
          </React.Fragment>
        </View>
        <Divider style={styles.divider}></Divider>

        <View style={styles.rowContainer}>
          <Text category="h5" style={{ flex: 1, textAlign: 'left' }}>Pesanan</Text>
        </View>
        <View style={styles.rowContainer}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.8 }}>
              <Text style={{ marginTop: 8 }}>ID Order</Text>
              <Text style={{ marginTop: 8 }}>Nama</Text>
              <Text style={{ marginTop: 8 }}>Email</Text>
              <Text style={{ marginTop: 8 }}>Telepon</Text>
              <Text style={{ marginTop: 8 }}>Media Pembayaran</Text>
              <Text style={{ marginTop: 8 }}>Tanggal Transaksi</Text>
              <Text style={{ marginTop: 8 }}>Tanggal Garansi</Text>

            </View>
            <View style={{ flex: 1 }}>
              <Text category='s1' style={{ marginTop: 8 }}>#ORD420210213</Text>
              <Text category='s1' style={{ marginTop: 8 }}>Arya</Text>
              <Text category='s1' style={{ marginTop: 8 }}>aryasetyanovanto@gmail.com</Text>
              <Text category='s1' style={{ marginTop: 8 }}>08567775758</Text>
              <Text category='s1' style={{ marginTop: 8 }}>Cash</Text>
              <Text category='s1' style={{ marginTop: 8 }}>2021-02-11</Text>
              <Text category='s1' style={{ marginTop: 8 }}>2021-03-11</Text>
            </View>
          </View>
        </View>

        <Divider style={styles.divider}></Divider>

        <View style={styles.rowContainer}>
          <Text category="h5" style={{ flex: 1, textAlign: 'left' }}>Pembayaran</Text>
        </View>
        <View style={styles.rowContainer}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.8 }}>
              <Text style={{ marginTop: 8 }}>Sub Total</Text>
              <Text style={{ marginTop: 8 }}>Diskon</Text>
              <Text style={{ marginTop: 8 }}>Total</Text>

            </View>
            <View style={{ flex: 1 }}>
              <Text category='s1' style={{ marginTop: 8 }}>#IDR  10,800,000</Text>
              <Text category='s1' style={{ marginTop: 8 }}>0</Text>
              <Text category='s1' style={{ marginTop: 8 }}>#IDR  10,800,000</Text>

            </View>
          </View>
        </View>
      </ScrollView>

    </View >




  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },

  dateContainer: {
    width: '100%',
    marginVertical: 10,
    marginHorizontal: 20
  },

  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 20
  },

  topContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  card: {
    flex: 1,
    margin: 2,
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  footerControl: {
    marginHorizontal: 2,
  },

  icon: {
    width: 32,
    height: 32,
    marginBottom: 12
  },

  button: {
    margin: 2,
    borderColor: 'transparent'
  },
  controlContainer: {
    borderRadius: 4,
    margin: 2,
    padding: 6,
    justifyContent: 'center',
    backgroundColor: '#3366FF',
  },

  divider: {
    width: '100%',
    height: 5,
    marginVertical: 15
  },

  // shop: {
  //   flex: 1,
  //   textAlign: 'center',
  //   marginBottom: 10
  // },
  // button: {
  //   flex: 1,
  //   marginTop: 20,
  // },
  // headerLeft: {
  //   color: '#01573c'
  // },
  // logo: {
  //   width: '100%',
  //   resizeMode: 'contain',
  //   marginTop: 20,
  // },
  // title: {
  //   fontSize: 20,
  //   fontWeight: 'bold',
  //   flex: 1
  // },
  // separator: {
  //   marginVertical: 30,
  //   height: 1,
  //   width: '80%',
  // },
  // rowContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  // },
  // headerContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  //   backgroundColor: '#fff',
  //   width: '100%'
  // },
  // containerLayout: {
  //   flexDirection: 'row',
  //   flexWrap: 'wrap',
  // },
  // cardHeaderTop: {
  //   flex: 1,
  //   marginHorizontal: 20,
  //   marginTop: -30,
  //   backgroundColor: '#fff',
  // },
  // cardHeader: {
  //   flex: 1,
  //   marginHorizontal: 20,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
  // menuLeft: {
  //   flex: 1,
  //   marginLeft: 20,
  //   marginRight: 10,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
  // menuRight: {
  //   flex: 1,
  //   marginRight: 20,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
});
