import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Text, Input, Layout, Icon, List, Datepicker, ListItem, Button, TabView, Tab } from '@ui-kitten/components';
import { View } from '../../components/Themed';
import { BarNavigation, PageNavigation } from '../../components/TopNavigation';
import { UserContext } from '../../context/Auth/context';
import { OrderProvider } from '../../context/Order/OrderContext';
import { CategoryButton, OrderCard, ProductLayout } from '../../components/OrderComponent';
import { OrderContext } from '../../context/Order/context';
import { totalIcon, minusIcon, plusIcon, arrowIcon, searchIcon, formatNumber } from '../../components/Icon';
import { UsersContext } from '../../context/Users/context';
import Toast from 'react-native-toast-message';

const CalendarIcon = (props:any) => (
  <Icon {...props} name='calendar' />
);

const data = new Array(8).fill({
  title: 'Nama Customer',
  description: 'Tanggal Transaksi',
});

export default function OrderScreen(navigation: any) {
  const user = React.useContext(UsersContext)
  const [date, setDate] = React.useState(new Date())

  const renderItemAccessory = (props:any) => (
    <Button size='medium' onPress={() => navigation.navigation.navigate('DetailRekap', {
      tabBarVisibility: false,
    })}>Detail</Button>
  );



  const renderItem = ({ item, index }:any) => (
    <ListItem
      title={`${item.title} ${index + 1}`}
      description={`${item.description} ${index + 1}`}
      accessoryRight={renderItemAccessory}
    />

  );

  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const shouldLoadComponent = (index) => index === selectedIndex;
  const [order,setOrder] = React.useState([{
    
  }])
  const getOrder = async (param?: any) =>{
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/order' + new URLSearchParams(param).toString(), {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+user.state.token
          },
        }
      );
      let json = await response.json();
      console.log(json)
    } catch (error) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: 'yang itu',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  React.useEffect(() => {
    getOrder({})
  },[])

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <BarNavigation title="Pesanan" ></BarNavigation>
      <TabView
      selectedIndex={selectedIndex}
      shouldLoadComponent={shouldLoadComponent}
      onSelect={index => setSelectedIndex(index)}
      style={{width:'100%',marginTop:10}}
      >
        <Tab  title={evaProps => <Text category="h1" {...evaProps}>Dalam Proses</Text>}>
          <View style={styles.rowContainer}>
            <List
              style={styles.dateContainer}
              data={data}
              renderItem={renderItem}
            />
          </View>
        </Tab>
        <Tab  title={evaProps => <Text category="h1" {...evaProps}>Selesai</Text>}>
        <View style={styles.rowContainer}>
          <List
            style={styles.dateContainer}
            data={data}
            renderItem={renderItem}
          />
        </View>
        </Tab>
      </TabView>
      
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left'
  },

  dateContainer: {
    width: '110%',
    marginVertical: 10,
  },

  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 20
  },

  // shop: {
  //   flex: 1,
  //   textAlign: 'center',
  //   marginBottom: 10
  // },
  // button: {
  //   flex: 1,
  //   marginTop: 20,
  // },
  // headerLeft: {
  //   color: '#01573c'
  // },
  // logo: {
  //   width: '100%',
  //   resizeMode: 'contain',
  //   marginTop: 20,
  // },
  // title: {
  //   fontSize: 20,
  //   fontWeight: 'bold',
  //   flex: 1
  // },
  // separator: {
  //   marginVertical: 30,
  //   height: 1,
  //   width: '80%',
  // },
  // rowContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  // },
  // headerContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  //   backgroundColor: '#fff',
  //   width: '100%'
  // },
  // containerLayout: {
  //   flexDirection: 'row',
  //   flexWrap: 'wrap',
  // },
  // cardHeaderTop: {
  //   flex: 1,
  //   marginHorizontal: 20,
  //   marginTop: -30,
  //   backgroundColor: '#fff',
  // },
  // cardHeader: {
  //   flex: 1,
  //   marginHorizontal: 20,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
  // menuLeft: {
  //   flex: 1,
  //   marginLeft: 20,
  //   marginRight: 10,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
  // menuRight: {
  //   flex: 1,
  //   marginRight: 20,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
});
