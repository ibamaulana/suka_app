import * as React from 'react';
import {  StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Icon, Text,Tab, TabView, Layout, List, ListItem, Divider, CheckBox, Input, } from '@ui-kitten/components';
import { calendarIcon, ClockIcon, formatNumber, mapIcon } from '../../../components/Icon';
import { OrderLayout as OrderToko } from '../../../components/OrderComponent';
import { OrderLayout as OrderFoodCourt } from '../../../components/FoodcourtComponent';
import { OrderContext } from '../../../context/Order/context';
import { UsersContext } from '../../../context/Users/context';
import { FoodcourtContext } from '../../../context/Foodcourt/context';
import moment from 'moment';
import * as Linking from 'expo-linking'
import { LoadingScreen } from '../../../components/GlobalComponent';

export const FormBayar = (props:any) => {
  const user = React.useContext(UsersContext)
  return (
  <>
    <Layout style={styles.inputContainer} level="1">
      <Input
        label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>Nama</Text>}
        style={styles.input}
        value={user.state.user.name}
        size="small"
      />
    </Layout>
    <Layout style={styles.inputContainer} level="1">
      <Input
        label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>No. Handphone</Text>}
        style={styles.input}
        size="small"
      />
    </Layout>
    <Layout style={styles.inputContainer} level="1">
      <Input
        label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>Email</Text>}
        style={{...styles.input,color:'#111'}}
        value={user.state.user.email}
        size="small"
      />
    </Layout>
    
  </>
  )
}

export default function BayarScreenToko
(props?: any) {
  const order = React.useContext(OrderContext)
  const foodcourt = React.useContext(FoodcourtContext)
  const users = React.useContext(UsersContext)
  const [isLoading,setLoading] = React.useState(false)

  const Bayar = () => {
    setLoading(true)
    let order_detail = []
    order.state.order.map((e:any) => {
      order_detail.push({
        product_name: e.product_name,
        qty:e.qty,
        price:e.total_price,
        date_start: moment().format('Y-m-d'),
        date_end: moment().format('Y-m-d'),
        hour_start: moment().format('h:i'),
        hour_end: moment().format('h:i')
      })
    })
   
    //midtrans
    let redirectUrl = Linking.createURL('Berhasil', {
    });
    let param = {
      "lapangan" : null,
      "order_type" : "produk",
      "service_type" : "umum",
      "user_type" : 'umum',
      "payment_type" : "lunas",
      "member_number" : null,
      "payment_method": "midtrans",
      "nama_corporate" : null,
      "berkas_corporate": null,
      "down_payment": 0,
      "subtotal": order.state.total,
      "discount": 0,
      "total" : order.state.total,
      "is_midtrans": true,
      "midtrans_callback": redirectUrl,
      "order_detail" : order_detail
    }
    fetch(
      'https://uin.vantura.id/api/order/create',{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+users.state.token
        },
        body: JSON.stringify(param)
      }
    )
    .then(response => response.json())
    .then(data => {
      setLoading(false)
       if(props.route.params.section === 'toko'){
        order.dispatch({type:'payment'})
      }else{
        foodcourt.dispatch({type:'payment'})
      }
       props.navigation.navigate('Payment',{
        tabBarVisibility: false,
        url:data.midtrans.data.redirect_url
      })
    })
    .catch((error) => {
      console.error('Error:', error);
    });
    // props.navigation.navigate('Berhasil',{
    //   tabBarVisibility: false,
    // })
  }
  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Tinjauan Pemesanan" {...props}></PageNavigation>
      {
        isLoading && (
          <LoadingScreen/>
        )
      }
      <ScrollView style={{width:'100%',height:100,marginTop:10,marginBottom:110}}>
        <FormBayar></FormBayar>
        <Divider style={styles.dividers}></Divider>
        <Layout style={styles.tabContainer}>
          <View style={{...styles.rowContainer,paddingBottom:20}}>
            <Text category="h5" style={{flex:1,textAlign:'left',fontSize:18}}>
            Pesanan</Text>
            <Button appearance="outline" size="small">Ubah Pesanan</Button>
          </View>
          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Divider style={{height:2,width:'100%'}}/>
          </View>
         {
           props.route.params.section === 'toko' && (
            <OrderToko/>

           )
         }
         {
           props.route.params.section === 'foodcourt' && (
             <OrderFoodCourt />
           )
         }

          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Divider style={{height:2,width:'100%'}}/>
          </View>

          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Text category="h4" style={{flex:1,textAlign:'left',fontSize:18}}>
            Total</Text>
            {props.route.params.section === 'toko' && (
              <Text category="s1" style={{flex:1,textAlign:'right',fontSize:14}}>
              Rp {formatNumber(order.state.total)}</Text>
            )}
            {props.route.params.section === 'foodcourt' && (
              <Text category="s1" style={{flex:1,textAlign:'right',fontSize:14}}>
              Rp {formatNumber(foodcourt.state.total)}</Text>
            )}
          </View>
        </Layout>
        {/* <Divider style={styles.dividers}></Divider> */}
         
      </ScrollView>
      <View style={{...styles.containerPay,paddingBottom:20}}>
          <Button onPress={() => Bayar()} style={{...styles.payButton}} size='giant'>
            Bayar
          </Button>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
   
  },
  payButton:{
    width:'90%',
    backgroundColor:'#01573c',
    borderColor: '#01573c',
    alignSelf:'center',
    marginVertical:10
  },
  containerPay: {
    bottom:0,
    marginHorizontal:0,
    height:100,
    width:'100%',
    position:'absolute',
    backgroundColor:'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  dividers:{
		width:'100%',
		height:5,
		marginVertical:20,
		alignSelf:'center'
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left',
    color: 'black'
  },
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
