import * as React from 'react';
import { StyleSheet,Dimensions,Image } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Divider,Text,Button,Modal,Card, Tab, TabView,Layout } from '@ui-kitten/components';
import { View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { OrderLayout,PayCard } from '../../../components/OrderComponent';
import { ScrollView } from 'react-native-gesture-handler';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { FormPelanggan, PayPanel, PaySummary, PromoPanel } from '../../../components/PayComponent';
import { LoadingScreen } from '../../../components/GlobalComponent';
import { FormFilter } from '../../../components/ReportComponent';

export default function RekapScreen(props:any) {
  const [loading,setLoading] =  React.useState(false)
  const [modal,setModal] =  React.useState(false)

  let refs = Array.from({length: 2}, () => React.createRef<SlidingUpPanel>())
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  const shouldLoadComponent = (index:any) => index === selectedIndex;
  return (
    <>
      <View style={styles.container}>
        <StatusBar style="light" />
        <PageNavigation title="Rekap" {...props}></PageNavigation>
        <TabView
          selectedIndex={selectedIndex}
          shouldLoadComponent={shouldLoadComponent}
          onSelect={index => setSelectedIndex(index)}
          style={{width:'100%',marginTop:10}}
          >
          <Tab  title={evaProps => <Text category="h1" {...evaProps}>Transaksi</Text>}>
            <ScrollView style={{marginTop:10}}>
              <FormFilter></FormFilter>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Transaksi</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Laba Kotor</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <Divider style={styles.divider}></Divider>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Void</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Diskon</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Tax</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Service</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <Divider style={styles.divider}></Divider>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Laba Bersih</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Rata Rata</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
            </ScrollView>
          </Tab>
          <Tab title={evaProps => <Text category="h1" {...evaProps}>Pembayaran</Text>}>
          <ScrollView style={{marginTop:10}}>
              <FormFilter></FormFilter>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Transaksi</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Laba Kotor</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <Divider style={styles.divider}></Divider>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Void</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Diskon</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Tax</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Service</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <Divider style={styles.divider}></Divider>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Laba Bersih</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Rata Rata</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
            </ScrollView>
          </Tab>
          <Tab title={evaProps => <Text category="h1" {...evaProps}>Produk</Text>}>
            <ScrollView style={{marginTop:10}}>
              <FormFilter></FormFilter>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Transaksi</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Laba Kotor</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <Divider style={styles.divider}></Divider>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Void</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Diskon</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Tax</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total Service</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <Divider style={styles.divider}></Divider>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Laba Bersih</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
              <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Rata Rata</Text>
                <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
              </View>
            </ScrollView>
          </Tab>
        </TabView>
      </View>
    </>
   );
}

const styles = StyleSheet.create({
  tabContainer: {
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
	payButton:{
    flex:1,
    alignSelf:'center',
    marginVertical:10
  },
	subView: {
		bottom:0,
		borderTopLeftRadius:14,
		borderTopRightRadius:14,
		paddingTop:15,
  },
	pilihBtn:{
		flex:1,
		marginHorizontal:5
	},
	btn:{
		alignSelf:'flex-end',
    width:150,
	},
	ubahButton:{
    alignSelf:'flex-end',
    width:150,
		backgroundColor:'#506690',
		borderColor:'#506690',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
  },
	inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:10,
    marginHorizontal:20
  },
  input:{
    flex:1,
  },
  divider:{
      width:'100%',
      height:5,
      marginVertical:15
  },
  backdrop:{
    backgroundColor:'#506690',
    opacity:0.5,
    height:'100%',
    width:'100%'
  },
  modalCard:{
    alignItems:'center',
    flex:1,
  },
  logo:{ 
    width: 100,
    height:100,
    resizeMode:'contain',
    marginBottom:10
  },
	
});
