import * as React from 'react';
import {  Image, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { ImageCarousel } from '../../../components/Carousel';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Icon, Text,Tab, TabView, Layout, Divider, } from '@ui-kitten/components';
import { calendarIcon, ClockIcon, mapIcon } from '../../../components/Icon';
import { ClubContext } from '../../../context/Clubhouse/context';

export default function ClubhouseScreen
(props?: any) {
  const club = React.useContext(ClubContext)
  const [index,setIndex] = React.useState(0)
  React.useEffect(() => {
    club.dispatch({type:'reset-club'})
  },[])
  const item = [
    {
      image: require('../../../assets/icon/club-uin.jpeg')
    },
    {
      image: require('../../../assets/icon/club-uin-2.jpeg')
    },
    {
      image: require('../../../assets/icon/club-uin-3.jpeg')
    },
    
  ];
  const layanan = ['reguler','turnamen']
  const layananList = [
    {
      label:'Umum',
      value:'umum',
    },
    {
      label:'Internal UIN Suka',
      value:'internal',
    },
  ];
  const setLayanan = (value:any) => {
    club.dispatch({type:'update-field',field:'layanan_index',value:value})
    club.dispatch({type:'update-field',field:'layanan',value:layanan[value]})
    club.dispatch({type:'update-field',field:'status_layanan',value:'umum'})
  }
  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Clubhouse" {...props}></PageNavigation>
      <ImageCarousel item={item}></ImageCarousel>
      <ScrollView style={{width:'100%',height:100,marginTop:10}}>
        <View style={{...styles.rowContainer,paddingTop:10,marginVertical:10}}>
          <Text category="h5" style={{fontSize:18}}>Clubhouse UIN Sunan Kalijaga</Text>
        </View>
        <View style={{...styles.rowContainer,paddingTop:10}}>
          <Text category="s1" style={{flex:1,textAlign:'left',fontSize:17}}>
          Jl. Bimo Kurdo No.25, Papringan, Caturtunggal, Kec. Depok, Kab. Sleman, D.I. Yogyakarta, 55281</Text>
          <Button style={styles.button} appearance='outline'  size="small" accessoryLeft={mapIcon}>
            Map
          </Button>
        </View>
        <Divider style={styles.dividers}></Divider>
        <View style={{...styles.rowContainer,paddingTop:0,marginVertical:10}}>
          <Text category="h5" style={{fontSize:18}}>Fasilitas</Text>
        </View>
        <View style={{...styles.rowContainer,paddingTop:10,marginVertical:10}}>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/wifi.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>WiFi</Text>
          </View>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/ac.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>AC</Text>
          </View>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/water-heater.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>Water Heater</Text>
          </View>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/shower.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>Shower</Text>
          </View>
        </View>
        <View style={{...styles.rowContainer,paddingTop:10,marginVertical:10}}>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/bed.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>King Size Bed</Text>
          </View>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/tv.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>TV</Text>
          </View>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/desk.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>Writing Desk</Text>
          </View>
          <View style={{flex:1,height:50}}>
            <Image source={require('../../../assets/icon/sofa.png')} style={{height:30,width:'auto',resizeMode:'contain'}}/>
            <Text category="s1" style={{fontSize:12,textAlign:'center',paddingTop:10}}>Sofa</Text>
          </View>
        </View>
        <Divider style={styles.dividers}></Divider>

        <TabView
            selectedIndex={index}
            onSelect={index => setIndex(index)}
        >
            <Tab  title={evaProps => <Text category="h1" {...evaProps}>Ketentuan Pemesanan</Text>}>
              <Layout style={styles.tabContainer}>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingVertical:20}}>
                  <Text category="h5" style={{flex:1,textAlign:'left',fontSize:17}}>
                  Ketentuan Pemesanan</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  1. Sewa Club House sesuai tarif yang berlaku.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  2. Pemesanan lebih dari 6 kamar dikenakan Down Payment (DP) 25% dari total harga sewa.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  3. Down Payment (DP) tidak dapat dikembalikan jika pemesanan dibatalkan.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  4. Check In dilakukan pada pukul 14.00 WIB dan Check Out pada pukul 13.00 WIB.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  5. Apabila Check In sebelum pukul 05.00 WIB, maka sewa kamar akan dihitung satu hari.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  6. Untuk penambahan masa tinggal sampai dengan pukul 18.00 WIB, sewa kamar akan dikenakan biaya over time sebesar 50%.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  7. Check Out di atas pukul 18.00 WIB akan dikenakan tarif penuh.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7'}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  8. Bagi Organisasi Mahasiswa wajib menyertakan surat kegiatan resmi dan rekomendasi dari Wakil Rektor 3 atau Wakil Dekan 3.</Text>
                </View>
              </Layout>
            </Tab>
            <Tab title={evaProps => <Text category="h1" {...evaProps}>Tata Tertib</Text>}>
              <Layout style={styles.tabContainer}>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingVertical:20}}>
                  <Text category="h5" style={{flex:1,textAlign:'left',fontSize:17}}>
                  Tata Tertib</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  1. Sewa Club House sesuai tarif yang berlaku.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  2. Pemesanan lebih dari 6 kamar dikenakan Down Payment (DP) 25% dari total harga sewa.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  3. Down Payment (DP) tidak dapat dikembalikan jika pemesanan dibatalkan.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  4. Check In dilakukan pada pukul 14.00 WIB dan Check Out pada pukul 13.00 WIB.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  5. Apabila Check In sebelum pukul 05.00 WIB, maka sewa kamar akan dihitung satu hari.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  6. Untuk penambahan masa tinggal sampai dengan pukul 18.00 WIB, sewa kamar akan dikenakan biaya over time sebesar 50%.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  7. Check Out di atas pukul 18.00 WIB akan dikenakan tarif penuh.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7'}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  8. Bagi Organisasi Mahasiswa wajib menyertakan surat kegiatan resmi dan rekomendasi dari Wakil Rektor 3 atau Wakil Dekan 3.</Text>
                </View>
              </Layout>
            </Tab>
          </TabView>
          <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:30,alignItems:'flex-end',height:100}}>
            <Button onPress={() => props.navigation.navigate('Pesan',{
            tabBarVisibility: false,
          })}  style={{...styles.button,flex:1}} size='giant'>
              Periksa Ketersediaan
            </Button>
          </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    height: 410,
    backgroundColor:'#F7F7F7'
  },
  dividers:{
		width:'100%',
		height:5,
		marginVertical:20,
		alignSelf:'center'
  },
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
