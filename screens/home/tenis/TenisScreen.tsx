import * as React from 'react';
import {  Linking, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { ImageCarousel } from '../../../components/Carousel';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Icon, Text,Tab, TabView, Layout, } from '@ui-kitten/components';
import { calendarIcon, ClockIcon, mapIcon } from '../../../components/Icon';
import { SelectForm } from '../../../components/SelectForm';
import { FutsalContext } from '../../../context/Futsal/context';

export default function TenisScreen
(props?: any) {
  const futsal = React.useContext(FutsalContext)
  React.useEffect(() => {
    futsal.dispatch({type:'reset-tenis'})
  },[])
  const item = [
    {
      image: require('../../../assets/icon/tenis-1.jpg')
    },
    {
      image: require('../../../assets/icon/tenis-2.jpg')
    },
    {
      image: require('../../../assets/icon/tenis-3.jpg')
    },
    
  ];
  const layanan = ['reguler','turnamen']
  const setLayanan = (value:any) => {
    futsal.dispatch({type:'update-field',field:'layanan_index',value:value})
    futsal.dispatch({type:'update-field',field:'layanan',value:layanan[value]})
    futsal.dispatch({type:'update-field',field:'status_layanan',value:'umum'})
  }
  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Lapangan Tenis" {...props}></PageNavigation>
      <ImageCarousel item={item}></ImageCarousel>
      <ScrollView style={{width:'100%',height:100,marginTop:10}}>
        <View style={{...styles.rowContainer,paddingTop:10}}>
          <Text category="s1" style={{flex:1,textAlign:'left',fontSize:17}}>
           Jl. Timoho No.64 C, Papringan, Caturrtunggal ...</Text>
        </View>
        <View style={{...styles.rowContainer,paddingTop:5}}>
          <View style={{ flexDirection: 'row',alignItems:'center' }}>
            <ClockIcon fill='black' width={20} height={20}></ClockIcon>
            <Text category="s1" style={{marginLeft:5,fontSize:14}}>11:00 - 21:00</Text>
          </View>
          <Button onPress={() => Linking.openURL('https://goo.gl/maps/ZXU4PkaTBxQfPmvbA')} style={styles.button} appearance='outline'  size="small" accessoryLeft={mapIcon}>
            Peta
          </Button>
        </View>
        <View style={{...styles.rowContainer,paddingTop:10,marginVertical:10}}>
          <Text category="h5" style={{fontSize:18}}>Pilih Layanan</Text>
        </View>
        <TabView
            selectedIndex={futsal.state.layanan_index}
            onSelect={index => setLayanan(index)}
        >
            <Tab  title={evaProps => <Text category="h1" {...evaProps}>REGULER</Text>}>
              <Layout style={styles.tabContainer}>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingVertical:20}}>
                  <Text category="h5" style={{flex:1,textAlign:'left',fontSize:17}}>
                  Syarat dan Ketentuan</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  1. Seluruh pemain wajib mematuhi protokol kesehatan yang telah ditetapkan oleh pemerintah dan ketua satgas Covid-19 setempat.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  2. Merusak properti lapangan wajib mengganti seharga properti yang dirusak.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  3. Kecelakan / kerusakan yang terjadi akibat dari penggunaan lapangan futsal dan tenis adalah diluar tanggung jawab Badan Usaha..</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  4. <Text category="s1" status='danger'>Pembatalan Pesanan</Text> hanya bisa dilakukan untuk layanan jenis TURNAMEN.</Text>
                </View>
              </Layout>
            </Tab>
            <Tab title={evaProps => <Text category="h1" {...evaProps}>TURNAMEN</Text>}>
              <Layout style={styles.tabContainer}>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingVertical:20}}>
                  <Text category="h5" style={{flex:1,textAlign:'left',fontSize:17}}>
                  Syarat dan Ketentuan</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  1. Seluruh pemain wajib mematuhi protokol kesehatan yang telah ditetapkan oleh pemerintah dan ketua satgas Covid-19 setempat.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  2. Merusak properti lapangan wajib mengganti seharga properti yang dirusak.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  3. Kecelakan / kerusakan yang terjadi akibat dari penggunaan lapangan futsal dan tenis adalah diluar tanggung jawab Badan Usaha..</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  4. <Text category="s1" status='danger'>Pembatalan TURNAMEN</Text> bisa diajukan maksimal 1 minggu sebelum hari pelaksanaan TURNAMEN.</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  5. Apabila terdapat <Text category="s1" status='danger'>Pembatalan TURNAMEN</Text>, akan ada pemotongan biaya  administrasi sebesar 50% dari nominal pembayaran down payment (DP).</Text>
                </View>
                <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:13}}>
                  6. Sewa Lapangan minimal 6 jam.</Text>
                </View>
              </Layout>
            </Tab>
          </TabView>
          <View style={{...styles.rowContainer,backgroundColor:'#F7F7F7',paddingBottom:30,alignItems:'flex-end',height:100}}>
            <Button onPress={() => props.navigation.navigate('PesanTenis',{
            tabBarVisibility: false,
          })}  style={{...styles.button,flex:1}} size='giant'>
              Buat Pesanan
            </Button>
          </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    height: 320,
    backgroundColor:'#F7F7F7'
  },
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
