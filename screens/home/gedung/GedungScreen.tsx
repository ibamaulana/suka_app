import * as React from 'react';
import {  Image, Linking, StyleSheet, TouchableOpacity } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { ImageCarousel } from '../../../components/Carousel';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Icon, Text,Tab, TabView, Layout, Divider, Card, } from '@ui-kitten/components';
import { calendarIcon, ChevronDown, ChevronUp, ClockIcon, mapIcon } from '../../../components/Icon';
import { ClubContext } from '../../../context/Clubhouse/context';
import { Toggle } from '../../../components/GlobalComponent';

export default function GedungScreen
(props?: any) {
  const club = React.useContext(ClubContext)
  const [index,setIndex] = React.useState(0)
  React.useEffect(() => {
    club.dispatch({type:'reset-club'})
  },[])
  const item = [
    {
      image: require('../../../assets/icon/gedung-1.jpg')
    },
    {
      image: require('../../../assets/icon/gedung-2.jpg')
    },
    {
      image: require('../../../assets/icon/gedung-3.jpg')
    },
    
  ];
  const layanan = ['reguler','turnamen']
  const layananList = [
    {
      label:'Umum',
      value:'umum',
    },
    {
      label:'Internal UIN Suka',
      value:'internal',
    },
  ];
  const setLayanan = (value:any) => {
    club.dispatch({type:'update-field',field:'layanan_index',value:value})
    club.dispatch({type:'update-field',field:'layanan',value:layanan[value]})
    club.dispatch({type:'update-field',field:'status_layanan',value:'umum'})
  }

  const ketentuan_pemesanan = [
    'Melakukan pembayaran Down Payment (DP) sebesar 20% dari total harga sewa.',
    'Pembayaran Down Payment (DP) paling lambat dilakukan 2 jam setelah pemesanan',
    'Apabila belum ada pembayaran Down Payment (DP) maka gedung diperbolehkan ditawarkan kepada konsumen yang lain',
    'Pelunasan keseluruhan biaya dilakukan paling lambat 10 HARI sebelum hari H / pelaksanaan acara.',
    'Khusus untuk acara yang dipesan kurang dari 10 HARI, maka pembayaran gedung dilakukan sebesar 100% pada saat hari konfirmasi.',
    'Apabila penyewa menggunakan jasa catering dan dekorasi dari luar maka rekanan tersebut wajib menghubungi pihak PBB UIN Sunan Kalijaga.',
    'Kelebihan Pemakaian dikenakan biaya 20% per-jam dari tarif sewa.',
    'Harus disertai Fotokopi Kartu Keluarga dan Undangan Asli ( Bagi internal dan Alumni/Mahasiswa UIN Suka).'
  ];

  const ketentuan_pembatalan = [
    'Dikenakan Potongan Biaya Sebesar 50% dari Down Payment (DP), jika pembatalan dilakukan TIGA BULAN atau lebih sebelum acara diselenggarakan',
    'Down Payment (DP) tidak dapat dikembalikan, jika pembatalan dilakukan DUA BULAN sebelum acara diselenggarakan.',
    'Pemesanan atau pemakaian Gedung TIDAK DAPAT dialihkan kepada orang lain.'
  ];

  const ketentuan_penundaan = [
    'Penundaan / perubahan tanggal acara dapat dilakukan TIGA BULAN sebelum hari H acara.',
    'Untuk perubahan tanggal acara hanya dapat dilakukan TIGA BULAN sebelum hari H acara dan HANYA DAPAT DILAKUKAN SATU KALI PERUBAHAN TANGGAL ACARA. Perubahan tanggal acara hanya berlaku di tahun yang sama dan selama tanggal yang diinginkan masih tersedia.',
    'Untuk pembatalan, Anda wajib menghubungi pusat layanan konsumen Pusat Pengembangan Bisnis UIN SUKA.'
  ]

  const ketentuan_rekanan = [
    'Penyewa wajib mengunakan rekanan vendor catering, dekorasi Wedding Organizer (WO) Soundsystem yang terdaftar di pengelola Gedung Prof. Amin Abdullah.'
  ]

  const tata_tertib = [
    'Pengelola gedung tidak bertanggung jawab atas perubahan-perubahan yang dilakukan oleh pengguna gedung diluar kesepakatan kecuali adanya surat pemberitahuan resmi dari pengguna gedung maksimal 7 hari sebelum pelaksanaan acara',
    'Kelebihan makanan yang telah dipesan dapat dibawa pulang setelah acara berakhir.',
    'Kecelakaan maupun kelalaian dari pihak Vendor akan diselesaikan antara Vendor dan penyewa gedung tanpa harus melibatkan pihak pengelola.',
    'Pihak Penyewa atau Rekanan TIDAK DIPERKENANKAN merusak Lantai, Interior, memaku di dinding, gordyn, jendela, maupun pintu yang dapat merusak atau mengotori gedung serta perlengkapan yang ada. Kelalaian yang mengakibatkan kerusakan akan dikenakan ganti rugi sebesar biaya perbaikan.',
    'Pihak Penyewa atau RekananTIDAK DIPERBOLEHKAN membawa senjata api atau senjata tajam,minuman keras termasuk beer, obat-obatan terlarang kedalam lingkungan Gedung .',
    'Pihak Penyewa & Vendor DILARANG KERAS MEROKOK Termasuk di Hall Utama, Ruang Rias dan toilet. Merokok diperkenankan hanya di luar gedung.',
    'Pihak Penyewa atau Rekanan DILARANG MEMASAK yang membuat kotoran (sampah) berlebih di dalam area Gedung.',
    'Pihak Rekanan Dekorasi, Catering ataupun WO WAJIB Membersikan semua kotoran (sampah) yang timbul dan wajib membawa sampah tersebut atau dikenakan biaya charge untuk pembersihan sisa sampah dekorasi, catering atau WO.',
    'Pihak Vendor catering dan Dekorasi wajib melakukan loading dan persiapan sesuai dengan waktu yang telah ditentukan oleh pengelola gedung Prof Amin Abdullah. Jika terdapat kelebihan waktu dari yang sudah ditentukan, pihak vendor akan dikenakan charge sebesar Rp250.000 / jam',
    'Pihak Pengelola tidak bertanggung jawab terhadap acara yang tidak dikoordinasikan terlebih dahulu dan menyimpang dari ketentuan yang berlaku. Untuk itu apabila diperlukan pengelola bersedia untuk memberikan saran dalam hal penyelenggaran acara.',
    'Pihak Penyewa atau Rekanan TIDAK DIPERBOLEHKAN menggunakan saluran atau stop kontak listrik diluar yang telah ditentukan tanpa seiizin pengelola gedung.',
    'Pihak Penyewa & Rekanan dalam hal pemasangan spanduk,karangan bunga dan lain- lain demi menjaga ketertiban dan keindahan harus dikoordinasikan dengan pengelola gedung.',
    'Pihak Penyewa & Rekanan dilarang membawa, memindahkan/merubah peralatan milik gedung',
    'Pihak Penyewa atau Rekanan WAJIB mematuhi Tata Tertib dan Peraturan Gedung yang berlaku.'
  ]

  
  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Gedung Serbaguna" {...props}></PageNavigation>
      <ImageCarousel item={item}></ImageCarousel>
      <ScrollView style={{width:'100%',height:100,marginTop:10}}>
        <View style={{...styles.rowContainer,paddingTop:10,marginVertical:10}}>
          <Text category="h5" style={{fontSize:18}}>Gedung Serbaguna UIN Sunan Kalijaga</Text>
        </View>
        <View style={{...styles.rowContainer,paddingTop:10}}>
          <Text category="s1" style={{flex:1,textAlign:'left',fontSize:17}}>
          Jl. Timoho No.64C, Papringan, Caturtunggal, Kec. Depok, Kab. Sleman, D.I. Yogyakarta, 55281</Text>
          <Button onPress={() => Linking.openURL('https://goo.gl/maps/ZXU4PkaTBxQfPmvbA')} style={styles.button} appearance='outline'  size="small" accessoryLeft={mapIcon}>
            Peta
          </Button>
        </View>
        <Divider style={styles.dividers}></Divider>
          <Toggle
            title="Fasilitas"
          >
            <Layout style={{...styles.tabContainer,paddingTop:20}}>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                1. Kapasitas Halaman Utama:</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                - Standing Party 1.500</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                - Theater Style 750 – 1.000</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                - Sitting Party 20 s.d 35 table (6 Kursi/ Table)</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                2. AC Hall Sebesar 60 PK.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                3. AC Pelaminan/ Panggung 10 PK.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                4. Kursi sebanyak 750 Buah.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                5. Genset (standby apabila listrik mati).</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                6. 2 (Dua) Ruang Rias atau Transit (Ber AC, Toilet dalam).</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                7. Tempat Parkir Luas.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                8. Petugas Parkir, Petugas Kebersihan.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                9. Listrik tambahan 10.000 watt.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                10. Meja buku tamu 2 buah.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                11. LCD 2 Buah.</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                12. Biaya termasuk loading in dan loading out, tanpa AC</Text>
              </View>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                13. Waktu Pemakaian per hari 8 jam</Text>
              </View>
            </Layout>
          </Toggle>
        <Divider style={styles.dividers}></Divider>
        <Toggle
            title="Ketentuan Pemesanan, Pembatalan, Penundaan dan Rekanan"
          >
            <Layout style={{...styles.tabContainer,paddingTop:20}}>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:14}}>
                Ketentuan Pemesanan:</Text>
              </View>
              {
                ketentuan_pemesanan.map((e,key) => 
                  <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                    <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                    {key+1}. {e}</Text>
                  </View>
                )
              }

              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:14}}>
                Ketentuan Pembatalan:</Text>
              </View>
              {
                ketentuan_pembatalan.map((e,key) => 
                  <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                    <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                    {key+1}. {e}</Text>
                  </View>
                )
              }

              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:14}}>
                Ketentuan Penundaan:</Text>
              </View>
              {
                ketentuan_penundaan.map((e,key) => 
                  <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                    <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                    {key+1}. {e}</Text>
                  </View>
                )
              }

              <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:14}}>
                Ketentuan Rekanan:</Text>
              </View>
              {
                ketentuan_rekanan.map((e,key) => 
                  <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                    <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                    {key+1}. {e}</Text>
                  </View>
                )
              }
            </Layout>
          </Toggle>
        <Divider style={styles.dividers}></Divider>
        <Toggle
            title="Tata Tertib"
          >
          <Layout style={{...styles.tabContainer,paddingTop:20}}>
            {
              tata_tertib.map((e,key) => 
                <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:10}}>
                  <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
                  {key+1}. {e}</Text>
                </View>
              )
            }
          </Layout>
        </Toggle>
        <Divider style={styles.dividers}></Divider>
        <View style={{...styles.rowContainer,marginVertical:10}}>
          <Text category="h5" style={{fontSize:18}}>Pilih Layanan</Text>
        </View>
        <TouchableOpacity>
          <View style={{...styles.rowContainer,marginHorizontal:20,paddingHorizontal:0,marginVertical:10,...styles.shadowBorder}}>
            <Image source={require('../../../assets/icon/layanan-nikah.jpg')} style={{width:100,height:100,resizeMode:'cover',alignSelf:'flex-start',borderRadius:10}}/>
            <View style={{flex:1,marginLeft:10,marginRight:10}}>
              <Text category="h5" style={{fontSize:15,marginBottom:5,textAlign:'left'}}>Acara Pernikahan</Text>
              <Text category="s2" style={{fontSize:12,textAlign:'left'}}>(Untuk Umum, internal ,mahasiwa, 
              alumni, pensiunan UIN Suka)</Text>
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => props.navigation.navigate('PeriksaGedung',{
            tabBarVisibility: false,
          })}>
          <View style={{...styles.rowContainer,marginHorizontal:20,paddingHorizontal:0,marginVertical:10,marginBottom:30,...styles.shadowBorder}}>
            <Image source={require('../../../assets/icon/layanan-lainnya.jpg')} style={{width:100,height:100,resizeMode:'cover',alignSelf:'flex-start',borderRadius:10}}/>
            <View style={{flex:1,marginLeft:10,marginRight:10}}>
              <Text category="h5" style={{fontSize:15,marginBottom:5,textAlign:'left'}}>Acara Lainnya</Text>
              <Text category="s2" style={{fontSize:12,textAlign:'left'}}>(Wisuda Sekolah, Kejuaraan Olahraga,
              Pengajian, Gathering, Tes Pendidikan,
              Lainnya.)</Text>
            </View>
          </View>
        </TouchableOpacity>
         
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    height: 'auto',
    backgroundColor:'#fff'
  },
  dividers:{
		width:'100%',
		height:5,
		marginVertical:20,
		alignSelf:'center'
  },
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  icon:{
    flex:1,
    width:20,
    height:20,
    color:'#000'
  },
  shadowBorder:{
    borderWidth:1,
    borderColor:'#fff',
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  }
});
