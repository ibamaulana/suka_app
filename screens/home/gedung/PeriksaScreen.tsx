import * as React from 'react';
import {  StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import {  FormFilterGedung, FormFilterTenis } from '../../../components/PesananComponent';
import { Input, Layout } from '@ui-kitten/components';

export default function PeriksaGedungScreen
(props?: any) {

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Periksa Jadwal" {...props}></PageNavigation>
      <FormFilterGedung></FormFilterGedung>
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    height: 'auto',
    backgroundColor:'#f7f7f7'
  },
  
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
