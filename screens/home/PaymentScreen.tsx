import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Text, Input, Layout, Icon, List, Datepicker, ListItem, Button, TabView, Tab } from '@ui-kitten/components';
import { View } from '../../components/Themed';
import { BarNavigation, PageNavigation } from '../../components/TopNavigation';
import { UserContext } from '../../context/Auth/context';
import WebView from 'react-native-webview';

export default function PaymentScreen(props: any) {
  const user = React.useContext(UserContext)
  console.log(props)
  return (
    <>
     <StatusBar style="light" />
      <BarNavigation title="Payment" ></BarNavigation>
     <WebView
     style={styles.containerWeb}
     source={{ uri: props.route.params.url }}
   />
    </>
   
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
    },
    containerWeb: {
      height:'100%',
      width:'100%'
    },
});
