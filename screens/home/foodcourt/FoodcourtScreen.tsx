import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import {UserContext} from '../../../context/Auth/context';
import { Layout, Text } from '@ui-kitten/components';
import { OrderCard, ProductLayout } from '../../../components/FoodcourtComponent';

export default function FoodcourtScreen(props?: any) {
  const user = React.useContext(UserContext)

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Foodcourt" {...props}></PageNavigation>
      <ProductLayout user={user}></ProductLayout>
      <OrderCard navigation={props}></OrderCard>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
