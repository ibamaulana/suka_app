import * as React from 'react';
import {  Image, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import {UserContext} from '../../../context/Auth/context';
import { ImageCarousel } from '../../../components/Carousel';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Icon, Text,Tab, TabView, Layout, List, ListItem, Divider, CheckBox, Input, } from '@ui-kitten/components';

export default function BerhasilScreen
(props?: any) {
  const user = React.useContext(UserContext)
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const shouldLoadComponent = (index:any) => index === selectedIndex;
 

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      {/* <PageNavigation title="Bayar" {...props}></PageNavigation> */}
      <View style={{...styles.rowContainer}}>
        <Text category="h5" style={{flex:1,textAlign:'center',fontSize:25}}>
        Pemesanan Berhasil</Text>
      </View>
      <View style={{...styles.rowContainer,paddingVertical:20}}>
        <Image source={require('../../../assets/icon/check.png')}></Image>
      </View>
      <View style={{...styles.rowContainer}}>
        <Text category="s2" style={{flex:1,textAlign:'center',fontSize:18}}>
        Pemesanan anda berhasil diproses dan segera lakukan pembayaran</Text>
      </View>
      <View style={{...styles.rowContainer,paddingTop:20}}>
        <Button status="warning" size="large" style={{flex:1}}>Unggah Bukti Pemesanan</Button>
      </View>
      <View style={{...styles.rowContainer,paddingTop:20}}>
        <Button status="primary" size="large" style={{flex:1}}>Konfirmasi Pemesanan</Button>
      </View>
      <View style={{...styles.rowContainer,paddingTop:60}}>
        <Button status="basic" size="large" style={{flex:1}}
          onPress={() => props.navigation.navigate('HomeScreen',{
            tabBarVisibility: true,
          })}
        >Kembali ke Beranda</Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
   
  },
  dividers:{
		width:'100%',
		height:5,
		marginVertical:20,
		alignSelf:'center'
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left',
    color: 'black'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    alignContent:'center',
    justifyContent:'center',
    height:'100%'
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
