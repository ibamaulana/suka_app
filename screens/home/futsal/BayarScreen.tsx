import * as React from 'react';
import {  StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import {UserContext} from '../../../context/Auth/context';
import { ImageCarousel } from '../../../components/Carousel';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Icon, Text,Tab, TabView, Layout, List, ListItem, Divider, CheckBox, Input, } from '@ui-kitten/components';
import { calendarIcon, ClockIcon, mapIcon } from '../../../components/Icon';
import { FutsalContext } from '../../../context/Futsal/context';
import { FileUpload } from '../../../components/FileUpload';

export const FormBayar = (props:any) => {
  const futsal = React.useContext(FutsalContext)
  return (
  <>
    <Layout style={styles.inputContainer} level="1">
      <Input
        label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>Nama</Text>}
        style={styles.input}
        size="small"
      />
    </Layout>
    <Layout style={styles.inputContainer} level="1">
      <Input
        label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>No. Handphone</Text>}
        style={styles.input}
        size="small"
      />
    </Layout>
    <Layout style={styles.inputContainer} level="1">
      <Input
        label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>Email</Text>}
        style={styles.input}
        size="small"
      />
    </Layout>
    {
      futsal.state.status_layanan === 'internal' && (
        <Layout style={styles.inputContainer} level="1">
          <Input
            label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>NIK/NIM UIN Suka</Text>}
            caption="*khusus mahasiswa UIN Sunan Kalijaga Yogyakarta"
            style={styles.input}
            size="small"
          />
        </Layout>
      )
    }
    {
      futsal.state.status_layanan === 'corporate' && (
        <>
          <Layout style={styles.inputContainer} level="1">
            <Input
              label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>Nama Organisasi</Text>}
              style={styles.input}
              size="small"
            />
          </Layout>
          <Layout style={styles.inputContainer} level="1">
            <Text category='s1' style={{fontSize:13,marginVertical:5}}>Dokumen Organisasi</Text>
          </Layout>
          <FileUpload></FileUpload>
        </>
      )
    }
    {
      futsal.state.layanan === 'member' && (
        <Layout style={styles.inputContainer} level="1">
          <Input
            label={() => <Text category='s1' style={{fontSize:13,marginVertical:5}}>No. Keanggotaan</Text>}
            style={styles.input}
            size="small"
          />
        </Layout>
      )
    }
    
  </>
  )
}

export default function BayarScreen
(props?: any) {
  const futsal = React.useContext(FutsalContext)
  
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const shouldLoadComponent = (index:any) => index === selectedIndex;
 

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Tinjauan Pemesanan" {...props}></PageNavigation>
      <ScrollView style={{width:'100%',height:100,marginTop:10}}>
        <FormBayar></FormBayar>
        {/* <Divider style={styles.dividers}></Divider>
        <Layout style={styles.tabContainer}>
          <View style={{...styles.rowContainer}}>
            <Text category="h5" style={{flex:1,textAlign:'left',fontSize:18}}>
            Promo</Text>
            <Button appearance="outline" size="small">Pilih Promo</Button>
          </View>
        </Layout> */}
        <Divider style={styles.dividers}></Divider>
        <Layout style={styles.tabContainer}>
          <View style={{...styles.rowContainer,paddingBottom:20}}>
            <Text category="h5" style={{flex:1,textAlign:'left',fontSize:18}}>
            Pesanan</Text>
            <Button appearance="outline" size="small">Ubah Pesanan</Button>

          </View>
          <View style={{...styles.rowContainer,paddingBottom:10}}>
            <Text category="h4" style={{flex:1,textAlign:'left',fontSize:16,textTransform:'capitalize'}}>
            {futsal.state.layanan} - {futsal.state.status_layanan}</Text>
          </View>
          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Divider style={{height:2,width:'100%'}}/>
          </View>
         
          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
            Lapangan 1</Text>
          </View>

          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
            08.00 - 09.00</Text>
            <Text category="s1" style={{flex:1,textAlign:'right',fontSize:14}}>
           Rp. 100.000</Text>
          </View>
          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
            09.00 - 10.00</Text>
            <Text category="s1" style={{flex:1,textAlign:'right',fontSize:14}}>
           Rp. 100.000</Text>
          </View>
          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Text category="s1" style={{flex:1,textAlign:'left',fontSize:14}}>
            10.00 - 11.00</Text>
            <Text category="s1" style={{flex:1,textAlign:'right',fontSize:14}}>
           Rp. 100.000</Text>
          </View>

          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Divider style={{height:2,width:'100%'}}/>
          </View>

          <View style={{...styles.rowContainer,paddingVertical:10}}>
            <Text category="h4" style={{flex:1,textAlign:'left',fontSize:18}}>
            Total</Text>
            <Text category="s1" style={{flex:1,textAlign:'right',fontSize:14}}>
           Rp. 300.000</Text>
          </View>
        </Layout>
        <Divider style={styles.dividers}></Divider>

        <Layout style={styles.tabContainer}>
          <View style={{...styles.rowContainer,paddingBottom:10}}>
            <Text category="h5" style={{flex:1,textAlign:'left',fontSize:18}}>
            Media Pembayaran</Text>
            <Button appearance="outline" size="small">Bank Transfer</Button>
          </View>
        </Layout>
          <View style={{...styles.rowContainer,paddingBottom:20}}>
            <Button onPress={() => props.navigation.navigate('Berhasil',{
            tabBarVisibility: false,
          })} style={{...styles.button,flex:1}} size='giant'>
              Bayar
            </Button>
          </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
   
  },
  dividers:{
		width:'100%',
		height:5,
		marginVertical:20,
		alignSelf:'center'
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left',
    color: 'black'
  },
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
