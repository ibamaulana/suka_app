import * as React from 'react';
import {  StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import {UserContext} from '../../../context/Auth/context';
import { ImageCarousel } from '../../../components/Carousel';
import { ScrollView } from 'react-native-gesture-handler';
import { Button, Icon, Text,Tab, TabView, Layout, List, ListItem, Divider, CheckBox, } from '@ui-kitten/components';
import { calendarIcon, ClockIcon, mapIcon } from '../../../components/Icon';
import { FormFilter, FormFilterFutsal } from '../../../components/PesananComponent';
import { ListJadwal, ListJadwalFutsal } from '../../../components/ListJadwal';
import { FutsalContext } from '../../../context/Futsal/context';
import { FileUpload } from '../../../components/FileUpload';
import { UsersContext } from '../../../context/Users/context';
import Toast from 'react-native-toast-message';

export default function PesanScreen
(props?: any) {

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Pesan Lapangan" {...props}></PageNavigation>
      <FormFilterFutsal {...props}></FormFilterFutsal>
      
    </View>
  );
}

const styles = StyleSheet.create({
  tabContainer: {
    height: 'auto',
    backgroundColor:'#f7f7f7'
  },
  
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
});
