import * as React from 'react';
import { StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';

export default function PromoScreen(props:any) {
  return (
    <>
      <View style={styles.container}>
        <StatusBar style="light" />
        <PageNavigation title="Promo" {...props}></PageNavigation>
      </View>
    </>
   );
}

const styles = StyleSheet.create({
  tabContainer: {
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
	payButton:{
    flex:1,
    alignSelf:'center',
    marginVertical:10
  },
	subView: {
		bottom:0,
		borderTopLeftRadius:14,
		borderTopRightRadius:14,
		paddingTop:15,
  },
	pilihBtn:{
		flex:1,
		marginHorizontal:5
	},
	btn:{
		alignSelf:'flex-end',
    width:150,
	},
	ubahButton:{
    alignSelf:'flex-end',
    width:150,
		backgroundColor:'#506690',
		borderColor:'#506690',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
  },
	inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:10,
    marginHorizontal:20
  },
  input:{
    flex:1,
  },
  divider:{
      width:'100%',
      height:5,
      marginVertical:15
  },
  backdrop:{
    backgroundColor:'#506690',
    opacity:0.5,
    height:'100%',
    width:'100%'
  },
  modalCard:{
    alignItems:'center',
    flex:1,
  },
  logo:{ 
    width: 100,
    height:100,
    resizeMode:'contain',
    marginBottom:10
  },
	
});
