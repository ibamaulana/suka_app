import * as React from 'react';
import { Keyboard, KeyboardAvoidingView, Platform, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { FormShop } from '../../../components/Shop';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { LoadingComponent } from '../../../components/LoadingComponent';
import { UsersContext } from '../../../context/Users/context';

export default function InfoScreen(props:any) {
  const user = React.useContext(UsersContext)
  const [loading,setLoading] = React.useState({
    visible: false,
    status: null,
    message: null,
  })
  return (
    <>
    <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{width:'100%',height:'100%'}}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
        <View style={{height:'100%'}}>
          <StatusBar style="light" />
          <PageNavigation title="Informasi Toko" {...props}></PageNavigation>
          <LoadingComponent
            props={loading}
            setVisible={(val:any) => setLoading({...loading,visible:val})}
          />
          <ScrollView>
              <FormShop user={user} setLoading={(val:any) => setLoading({...loading,visible:val})}></FormShop>
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
    </>
   );
}
