import * as React from 'react';
import { Keyboard, KeyboardAvoidingView, Platform, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { FormShop } from '../../../components/ShopComponent';
import { UserContext } from '../../../context/Auth/context';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { Drawer, DrawerItem, Text } from '@ui-kitten/components';
import { drawerInfoIcon,drawerOutletIcon, forwardIcon, homeIcon, personIcon } from '../../../components/Icon';

export default function TokoScreen(props:any) {
  const [selectedIndex, setSelectedIndex]:any = React.useState(null);
  return (
    <>
    <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{width:'100%',height:'100%'}}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
        <View style={{height:'100%'}}>
          <StatusBar style="light" />
          <PageNavigation title="Toko" {...props}></PageNavigation>
          <Drawer
            selectedIndex={selectedIndex}
            onSelect={index => setSelectedIndex(index)}>
            <DrawerItem
              title={() => 
                <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Informasi Toko</Text>
              }
              
              accessoryRight={forwardIcon}
              onPress={() => props.navigation.navigate('Info',{
                tabBarVisibility: false,
              })}
              style={styles.drawer}
            />
            <DrawerItem
              title={() => 
                <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Outlet</Text>
              }
              
              accessoryRight={forwardIcon}
              onPress={() => props.navigation.navigate('Info',{
                tabBarVisibility: false,
              })}
              style={styles.drawer}
            />
            <DrawerItem
              title={() => 
                <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Pembayaran</Text>
              }
              
              accessoryRight={forwardIcon}
              onPress={() => props.navigation.navigate('Info',{
                tabBarVisibility: false,
              })}
              style={styles.drawer}
            />
            
          </Drawer>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
    </>
   );
}

const styles = StyleSheet.create({
  drawer:{
    paddingVertical:20, 
    fontSize:30
  },
  tabContainer: {
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
	payButton:{
    flex:1,
    alignSelf:'center',
    marginVertical:10
  },
	subView: {
		bottom:0,
		borderTopLeftRadius:14,
		borderTopRightRadius:14,
		paddingTop:15,
  },
	pilihBtn:{
		flex:1,
		marginHorizontal:5
	},
	btn:{
		alignSelf:'flex-end',
    width:150,
	},
	ubahButton:{
    alignSelf:'flex-end',
    width:150,
		backgroundColor:'#506690',
		borderColor:'#506690',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: "space-around"
  },
	inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:10,
    marginHorizontal:20
  },
  input:{
    flex:1,
  },
  divider:{
      width:'100%',
      height:5,
      marginVertical:15
  },
  backdrop:{
    backgroundColor:'#506690',
    opacity:0.5,
    height:'100%',
    width:'100%'
  },
  modalCard:{
    alignItems:'center',
    flex:1,
  },
  logo:{ 
    width: 100,
    height:100,
    resizeMode:'contain',
    marginBottom:10
  },
	
});
