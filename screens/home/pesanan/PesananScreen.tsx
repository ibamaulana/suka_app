import * as React from 'react';
import { StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Text  } from '@ui-kitten/components';
import {  View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { UserContext } from '../../../context/Auth/context';
import { CategoryButton, OrderCard, ProductLayout } from '../../../components/OrderComponent';
import { OrderProvider } from '../../../context/Order/OrderContext';

export default function PesananScreen(props:any) {
  const user = React.useContext(UserContext)
  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <PageNavigation title="Pesanan" {...props}></PageNavigation>
      <View style={styles.rowContainer}>
        <Text category="h5" style={{flex:1,textAlign:'left'}}>Pilih Produk</Text>
        <CategoryButton></CategoryButton>
      </View>
      <ProductLayout user={user}></ProductLayout>
      <OrderCard navigation={props}></OrderCard>
    </View>
   );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:10,
    marginHorizontal:20
  },
});
