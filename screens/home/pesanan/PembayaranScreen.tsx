import * as React from 'react';
import { StyleSheet,Dimensions,Image } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Divider,Text,Button,Modal,Card } from '@ui-kitten/components';
import { View } from '../../../components/Themed';
import { PageNavigation } from '../../../components/TopNavigation';
import { OrderLayout,PayCard } from '../../../components/OrderComponent';
import { ScrollView } from 'react-native-gesture-handler';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { FormPelanggan, PayPanel, PaySummary, PromoPanel } from '../../../components/PayComponent';
import { LoadingScreen } from '../../../components/GlobalComponent';

export default function PembayaranScreen(props:any) {
  const [loading,setLoading] =  React.useState(false)
  const [modal,setModal] =  React.useState(false)

  let refs = Array.from({length: 2}, () => React.createRef<SlidingUpPanel>())

  return (
    <>
      {
        loading && (
          <LoadingScreen></LoadingScreen>
        )
      }
      <View style={styles.container}>
        <Modal
          visible={modal}
          backdropStyle={styles.backdrop}
          style={{width:'80%'}}
          >
          <Card disabled={true} style={{width:'100%'}}>
            <View style={styles.modalCard}>
              <Image source={require('../../../assets/icon/check.png')} style={styles.logo} /> 
              <Text category="h5" style={{marginBottom:50}}>Transaksi Berhasil !</Text>
              <View style={{flexDirection:'row',flexWrap:'nowrap'}}>
                <Button style={{flex:1,marginRight:10}} onPress={() => setModal(false)} status="warning">
                  Cetak Nota
                </Button>
                <Button style={{flex:1,marginLeft:10}} onPress={() => props.navigation.navigate('HomeScreen')} status="success">
                  Kembali 
                </Button>
              </View>
              
            </View>
            
          </Card>
        </Modal>
        <StatusBar style="light" />
        <PageNavigation title="Pembayaran" {...props}></PageNavigation>
				<ScrollView style={{marginBottom:100}}>
					<View style={styles.rowContainer}>
						<Text category="h5" style={{flex:1,textAlign:'left'}}>Pelanggan</Text>
					</View>
          <FormPelanggan></FormPelanggan>
					<Divider style={styles.divider}></Divider>
					<View style={styles.rowContainer}>
						<Text category="h5" style={{flex:1,textAlign:'left'}}>Produk</Text>
						<Button style={styles.ubahButton} size="small" onPress={() => props.navigation.navigate('Pesanan',{
										tabBarVisibility: false,
									})} appearance='filled' >
								Ubah Pesanan
						</Button>
					</View>
					<OrderLayout></OrderLayout>
					<Divider style={styles.divider}></Divider>
					<View  style={styles.rowContainer}>
					<Text category="h5" style={{flex:1,textAlign:'left'}}>Promo</Text>
						<Button style={styles.ubahButton} size="small" onPress={() => refs[0].current?.show()} appearance='filled'  >
								Pilih Promo
						</Button>
					</View>
					<Divider style={styles.divider}></Divider>
					<View style={styles.rowContainer}>
						<Text category="h5" style={{flex:1,textAlign:'left'}}>Summary</Text>
					</View>
					<PaySummary></PaySummary>
				</ScrollView>
			
				<PayCard slider={refs[1]}></PayCard>
				
        <PayPanel refs={refs} setLoading={setLoading} setModal={setModal}></PayPanel> 
				
        <PromoPanel refs={refs} ></PromoPanel>
      </View>
    </>
   );
}

const styles = StyleSheet.create({
	payButton:{
    flex:1,
    alignSelf:'center',
    marginVertical:10
  },
	subView: {
		bottom:0,
		borderTopLeftRadius:14,
		borderTopRightRadius:14,
		paddingTop:15,
  },
	pilihBtn:{
		flex:1,
		marginHorizontal:5
	},
	btn:{
		alignSelf:'flex-end',
    width:150,
	},
	ubahButton:{
    alignSelf:'flex-end',
    width:150,
		backgroundColor:'#506690',
		borderColor:'#506690',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
  },
	inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:10,
    marginHorizontal:20
  },
  input:{
    flex:1,
  },
  divider:{
      width:'100%',
      height:5,
      marginVertical:15
  },
  backdrop:{
    backgroundColor:'#506690',
    opacity:0.5,
    height:'100%',
    width:'100%'
  },
  modalCard:{
    alignItems:'center',
    flex:1,
  },
  logo:{ 
    width: 100,
    height:100,
    resizeMode:'contain',
    marginBottom:10
  },
	
});
