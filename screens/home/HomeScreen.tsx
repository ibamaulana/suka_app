import * as React from 'react';
import { Image,StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Layout,Button,Card,Text  } from '@ui-kitten/components';
import { View } from '../../components/Themed';
import { HomeNavigation } from '../../components/TopNavigation';
import { UserContext } from '../../context/Auth/context';
import { ScrollView } from 'react-native-gesture-handler';
import { plusIcon } from '../../components/Icon';
import { CardHeader, HomeCarousel } from '../../components/ReportComponent';
import { UsersContext } from '../../context/Users/context';
import Toast from 'react-native-toast-message';
import { env } from '../../config/environtment';
import { SelectForm } from '../../components/SelectForm';
import Dashboard from '../../components/Dashboard';

export default function HomeScreen(props?: any) {
  const user = React.useContext(UsersContext)

  return (
    <View style={styles.container}>

      <StatusBar style="light" />
      <HomeNavigation user={user.state.user}></HomeNavigation>
      <HomeCarousel {...props}></HomeCarousel>
      {/* <CardHeader {...props}></CardHeader> */}

      <ScrollView style={{width:'100%'}}>
      <Layout style={{...styles.rowContainer,marginHorizontal:20}} level='1'>
        <Text category="h4" style={{textAlign:'left',fontWeight:'700',flex:1,marginBottom:20}}>Layanan</Text>
      </Layout>
      <Layout style={{...styles.rowContainer,marginHorizontal:13}} level='1'>
        <View style={{flex:1,alignItems:'center'}}>
          <Card style={styles.menu}  onPress={() => props.navigation.navigate('Toko',{
              tabBarVisibility: false,
            })}>
              <Image source={require('../../assets/icon/toko.png')} style={styles.icon} /> 
          </Card>
          <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
            <Text category="label" style={{textAlign:'center',marginTop:20,flex:1,color: '#173655'}}>Toko</Text>
          </View>
        </View>

        <View style={{flex:1,alignItems:'center'}}>
        <Card style={styles.menu}  onPress={() => props.navigation.navigate('Foodcourt',{
            tabBarVisibility: false,
          })}>
            <Image source={require('../../assets/icon/food-court.png')} style={styles.icon} /> 
        </Card>
        <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
            <Text category="label" style={{textAlign:'center',marginTop:20,flex:1,color: '#173655'}}>Food Court</Text>
          </View>
        </View>

        <View style={{flex:1,alignItems:'center'}}>
          <Card style={styles.menu}  onPress={() => props.navigation.navigate('Futsal',{
              tabBarVisibility: false,
            })}>
            <Image source={require('../../assets/icon/futsal.png')} style={styles.icon} /> 
          </Card>
          <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="label" style={{textAlign:'center',marginTop:20,flex:1,color: '#173655'}}>Lap. Futsal</Text>
            </View>
        </View>

        
      </Layout>
      <Layout style={{...styles.rowContainer,marginHorizontal:13,marginTop:20}} level='1'>
        
        <View style={{flex:1,alignItems:'center'}}>
        <Card style={styles.menu}  onPress={() => props.navigation.navigate('Tenis',{
            tabBarVisibility: false,
          })}>
            <Image source={require('../../assets/icon/tennis.png')} style={styles.icon} /> 
        </Card>
        <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
            <Text category="label" style={{textAlign:'center',marginTop:20,flex:1,color: '#173655'}}>Lap. Tennis</Text>
          </View>
        </View>

        <View style={{flex:1,alignItems:'center'}}>
          <Card style={styles.menu}  onPress={() => props.navigation.navigate('Gedung',{
              tabBarVisibility: false,
            })}>
            <Image source={require('../../assets/icon/gedung.png')} style={styles.icon} /> 
          </Card>
          <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="label" style={{textAlign:'center',marginTop:20,flex:1,color: '#173655'}}>Gedung</Text>
            </View>
        </View>

        <View style={{flex:1,alignItems:'center'}}>
        <Card style={styles.menu}  onPress={() => props.navigation.navigate('Clubhouse',{
            tabBarVisibility: false,
          })}>
            <Image source={require('../../assets/icon/clubhouse.png')} style={styles.icon} /> 
        </Card>
        <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
            <Text category="label" style={{textAlign:'center',marginTop:20,flex:1,color: '#173655'}}>Clubhouse</Text>
          </View>
        </View>

        
      </Layout>
      {/* <Dashboard></Dashboard> */}
    </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    
    
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  icon:{ 
    width: 50,
    height: 50,
    resizeMode:'contain',
    alignSelf:'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-50,
    marginBottom:20,
    padding:20,
    backgroundColor: '#fff',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },

  menu: {
    backgroundColor: '#fff',
    borderWidth:1,
    borderRadius:10,
    height:100,
    width:100,
    justifyContent:'center',
  },
});
