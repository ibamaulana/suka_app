import * as React from 'react';
import { Image,StyleSheet,Alert } from 'react-native';
import EditScreenInfo from '../components/EditScreenInfo';
import { View } from '../components/Themed';
import { Input,Layout,Icon,Button,Spinner,Text } from '@ui-kitten/components';
import { TouchableWithoutFeedback } from 'react-native';
import Toast from 'react-native-toast-message';
import {UserContext} from '../context/Auth/context';
import * as SecureStore from 'expo-secure-store';
import { UsersContext } from '../context/Users/context';

export default function LoginScreen(props:any) {
  console.log(props)
  const user = React.useContext(UsersContext)
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (props?: any) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );

  const SubmitIcon = (props?: any) => (
    <Icon {...props} name='log-in-outline'/>
  );

  const LoadingIndicator = (props?: any) => (
      <Spinner status="primary" size='small'/>
  );

  const Login = async () => {
    if(email == '' || password == ''){
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: 'Email dan Password harus diisi !',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
      return false
    }
    setLoading(true)
    user.login({email,password}).then(e =>{
      setLoading(false)
    })
  }
  
  return (
    <View style={styles.container}>
      <Layout style={styles.rowContainer} level='1'>
        <Image source={require('../assets/images/logo.png')} style={styles.logo} /> 
      </Layout>
      <Text category="h5" style={{marginVertical:20}} >UIN SUKA Yogyakarta</Text>

      <Layout style={styles.rowContainer} level='1'>
        <Input
          label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Username</Text>}
          placeholder='Masukkan Email/NIK/NIP'
          value={email}
          onChangeText={nextValue => setEmail(nextValue)}
          style={styles.input}
        />
          
      </Layout>
      <Layout style={styles.rowContainer} level='1'>
        <Input
          value={password}
          label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Password</Text>}
          placeholder='Masukkan Password'
          // caption='Should contain at least 8 symbols'
          accessoryRight={renderIcon}
          // captionIcon={AlertIcon}
          secureTextEntry={secureTextEntry}
          onChangeText={nextValue => setPassword(nextValue)}
          style={styles.input}
        />
          
      </Layout>
      <Layout style={{...styles.rowContainer,marginTop:20}} level='1'>
        <Button style={styles.button} onPress={Login} disabled={loading} status='primary' accessoryLeft={!loading ? SubmitIcon : LoadingIndicator}>
          {!loading ? 'Login' : 'Loading...'}
        </Button>
      </Layout>
      <Layout style={{...styles.rowContainer,marginTop:20}} level='1'>
        <Text category="s1" style={{marginLeft:20}}>Belum punya akun ?</Text>
        <Button style={styles.button} onPress={() => props.navigation.navigate('Register',{
            tabBarVisibility: false,
          })} disabled={loading} status='basic' >
          Register
        </Button>
      </Layout>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo:{ 
    flex: 1,
    width: 100,
    height:100,
    resizeMode:'contain',
    marginTop:70
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input:{
    flex:1,
    marginHorizontal:20,
    marginVertical:4
  },
  button: {
    flex:1,
    marginHorizontal: 20,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#01573c',
    color: '#fff'
  },
});
