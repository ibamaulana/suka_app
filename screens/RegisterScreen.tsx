import * as React from 'react';
import { Image,StyleSheet,Alert } from 'react-native';
import EditScreenInfo from '../components/EditScreenInfo';
import { View } from '../components/Themed';
import { Input,Layout,Icon,Button,Spinner,Text, Modal, Card } from '@ui-kitten/components';
import { TouchableWithoutFeedback } from 'react-native';
import Toast from 'react-native-toast-message';
import {UserContext} from '../context/Auth/context';
import * as SecureStore from 'expo-secure-store';
import { UsersContext } from '../context/Users/context';
import { PageNavigation, PageNavigationLight } from '../components/TopNavigation';
import { SelectForm } from '../components/SelectForm';
import AwesomeAlert from 'react-native-awesome-alerts';

export default function RegisterScreen(props:any) {
  const user = React.useContext(UsersContext)
  const [loading, setLoading] = React.useState(false);
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (props?: any) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'}/>
    </TouchableWithoutFeedback>
  );

  const SubmitIcon = (props?: any) => (
    <Icon {...props} name='log-in-outline'/>
  );

  const LoadingIndicator = (props?: any) => (
      <Spinner status="primary" size='small'/>
  );

  const Register = async () => {
    if(jenis === 'umum'){
      if(param.email == '' || param.name == '' || param.password == ''){
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: 'Silahkan isi semua form!',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
        return false
      }
    }else{
      if(param.nik == '' || param.name == '' || param.password == ''){
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: 'Silahkan isi semua form!',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
        return false
      }
    }
    
    setLoading(true)
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/register', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({...param,status:jenis})
        }
      );
      let json = await response.json();
      setLoading(false)
      if(!json.success){
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: json.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }else{
        setAlert(true)
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Success',
          text2: 'Registrasi Berhasil!',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    } catch (error:any) {
      setLoading(false)
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: error.message,
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }
  const [showAlert,setAlert] = React.useState(false)
  const [jenis,setJenis] = React.useState('umum')
  const [param,setParam] = React.useState({
    email: '',
    nik: '',
    name: '',
    password:''
  })
  return (
    <View style={styles.container}>
       <Modal
        visible={showAlert}
        backdropStyle={styles.backdrop}
        onBackdropPress={() => setAlert(false)}>
        <Card disabled={true}>
          <View style={{...styles.rowContainer,marginBottom:10,alignItems:'center',alignSelf:'center'}}>
            <Image source={require('../assets/icon/check.png')}></Image>
          </View>
          <View style={{...styles.rowContainer,marginBottom:10,alignSelf:'center'}}>
            <Text category="h5" style={{textAlign:'center'}}>Pendaftaran berhasil!</Text>
          </View>
          <View style={{...styles.rowContainer,marginBottom:10,alignSelf:'center'}}>
            <Text category="s1" style={{textAlign:'center',fontSize:20}}>Silahkan cek email anda untuk melakukan verifikasi akun</Text>
          </View>
          <Button onPress={() => props.navigation.navigate('Login',{
              success_reg: true
            })}>
            Login
          </Button>
        </Card>
      </Modal>
      <PageNavigationLight title="Registrasi" {...props}></PageNavigationLight>
      <Layout style={styles.rowContainer} level='1'>
        <Input
          label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Nama</Text>}
          placeholder='Masukkan Nama'
          value={param.name}
          onChangeText={nextValue => setParam({...param,name:nextValue})}
          style={styles.input}
        />
      </Layout>
      <Layout style={styles.rowContainer} level='1'>
        <SelectForm
          value_list={[{label:'umum',value:'umum'},
          {label:'mahasiswa',value:'mahasiswa'},
          {label:'dosen',value:'dosen'}]}
          label="Pilih Jenis User"
          value={jenis}
          setValue={(value:any) => setJenis(value)}
        />
      </Layout>
      {
        jenis === 'umum' && (
        <Layout style={styles.rowContainer} level='1'>
          <Input
            label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Email</Text>}
            placeholder='Masukkan Email'
            value={param.email}
            onChangeText={nextValue => setParam({...param,email:nextValue})}
            style={styles.input}
          />
        </Layout>
        )
      }
      {
        jenis === 'mahasiswa' && (
          <Layout style={styles.rowContainer} level='1'>
            <Input
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>NIM</Text>}
              placeholder='Masukkan NIM'
              value={param.nik}
              onChangeText={nextValue => setParam({...param,nik:nextValue})}
              style={styles.input}
            />
          </Layout>
        )
      }
     {
        jenis === 'dosen' && (
          <Layout style={styles.rowContainer} level='1'>
            <Input
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>NIP</Text>}
              placeholder='Masukkan NIP'
              value={param.nik}
              onChangeText={nextValue => setParam({...param,nik:nextValue})}
              style={styles.input}
            />
          </Layout>
        ) 
      }
      <Layout style={styles.rowContainer} level='1'>
        <Input
          value={param.password}
          label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Password</Text>}
          placeholder='Masukkan Password'
          accessoryRight={renderIcon}
          secureTextEntry={secureTextEntry}
          onChangeText={nextValue => setParam({...param,password:nextValue})}
          style={styles.input}
        />
          
      </Layout>
      <Layout style={styles.rowContainer} level='1'>
        <Button style={styles.button} onPress={Register} disabled={loading} status='primary' accessoryLeft={!loading ? SubmitIcon : LoadingIndicator}>
          {!loading ? 'Registrasi' : 'Loading...'}
        </Button>
      </Layout>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  logo:{ 
    flex: 1,
    width: 100,
    height:100,
    resizeMode:'contain',
    marginTop:70
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  input:{
    flex:1,
    marginVertical:4
  },
  button: {
    flex:1,
    marginTop:20,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#01573c',
    color: '#fff'
  },
});
