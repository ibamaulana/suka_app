import * as React from 'react';
import { StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Text, Input, Layout, ListItem, Button,Drawer,DrawerItem, Avatar } from '@ui-kitten/components';
import { View } from '../../components/Themed';
import { BarNavigation, PageNavigation } from '../../components/TopNavigation';
import { forwardIcon } from '../../components/Icon';
import { UsersContext } from '../../context/Users/context';

export default function SettingScreen(navigation: any) {
  const user = React.useContext(UsersContext)
  const [date, setDate] = React.useState(new Date())
  const [selectedIndex, setSelectedIndex]:any = React.useState(null);

  const renderItemAccessory = (props:any) => (
    <Button size='medium' onPress={() => navigation.navigation.navigate('DetailRekap', {
      tabBarVisibility: false,
    })}>Detail</Button>
  );

  const level = (val:any) => {
    if(val == 1){
      return 'Admin';
    }else{
      return 'User';
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar style="light" />
      <BarNavigation title="Account" ></BarNavigation>
      <Layout
        level='2'
        style={styles.LayoutContainer}>
          <Avatar
          size="giant"
            style={styles.logo}
            source={require('../../assets/images/icon-user.png')}
          />
         <Text category='h3'style={{marginTop:20}}>{user.state.user.name}</Text>
        <Text category='h5'style={{fontWeight:"300"}}>{user.state.user.email}</Text>
        <Text category='h5'style={{fontWeight:"300"}}>{user.state.user.status}</Text>
      </Layout>
      <Layout
      style={styles.container}
      >
      <Drawer
        selectedIndex={selectedIndex}
        onSelect={index => setSelectedIndex(index)}
        style={{width:'100%'}}
      >
        {/* <DrawerItem
          title={() => 
            <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Edit Profil</Text>
          }
          accessoryRight={forwardIcon}
          onPress={() => navigation.navigation.navigate('Info',{
            tabBarVisibility: false,
          })}
          style={styles.drawer}
        /> */}
        <DrawerItem
          title={() => 
            <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Pengaturan Akun</Text>
          }
          accessoryRight={forwardIcon}
          style={styles.drawer}
        />
        <DrawerItem
          title={() => 
            <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Hubungi Layanan Konsumen</Text>
          }
          accessoryRight={forwardIcon}
          style={styles.drawer}
        />
         <DrawerItem
          title={() => 
            <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Pembatalan Pesanan</Text>
          }
          accessoryRight={forwardIcon}
          style={styles.drawer}
        />
        <DrawerItem
          title={() => 
            <Text category="p1" style={{marginLeft:10,fontWeight:'600',fontSize:20}}>Logout</Text>
          }
          accessoryRight={forwardIcon}
          onPress={() => user.logout()}
          style={styles.drawer}
        />
      </Drawer>
      </Layout>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width:'100%'
  },
  LayoutContainer: {
    alignItems: 'center',
    alignContent: 'space-between',
    width:'100%',
    paddingTop:'10%',
    paddingBottom:'10%'
  },
  drawer:{
    paddingVertical:20, 
    fontSize:30
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left'
  },

  dateContainer: {
    width: '110%',
    marginVertical: 10,
  },

  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 20
  },

  logo: {
    width: 100,
    height: 100
  },

  // shop: {
  //   flex: 1,
  //   textAlign: 'center',
  //   marginBottom: 10
  // },
  // button: {
  //   flex: 1,
  //   marginTop: 20,
  // },
  // headerLeft: {
  //   color: '#01573c'
  // },
  // logo: {
  //   width: '100%',
  //   resizeMode: 'contain',
  //   marginTop: 20,
  // },
  // title: {
  //   fontSize: 20,
  //   fontWeight: 'bold',
  //   flex: 1
  // },
  // separator: {
  //   marginVertical: 30,
  //   height: 1,
  //   width: '80%',
  // },
  // rowContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  // },
  // headerContainer: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  //   backgroundColor: '#fff',
  //   width: '100%'
  // },
  // containerLayout: {
  //   flexDirection: 'row',
  //   flexWrap: 'wrap',
  // },
  // cardHeaderTop: {
  //   flex: 1,
  //   marginHorizontal: 20,
  //   marginTop: -30,
  //   backgroundColor: '#fff',
  // },
  // cardHeader: {
  //   flex: 1,
  //   marginHorizontal: 20,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
  // menuLeft: {
  //   flex: 1,
  //   marginLeft: 20,
  //   marginRight: 10,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
  // menuRight: {
  //   flex: 1,
  //   marginRight: 20,
  //   marginTop: 20,
  //   backgroundColor: '#fff',
  // },
});
