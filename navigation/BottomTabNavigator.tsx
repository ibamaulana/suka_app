import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import HomeScreen from '../screens/home/HomeScreen';
import TabTwoScreen from '../screens/TabTwoScreen';
import { BottomTabParamList, TabOneParamList, TabTwoParamList } from '../types';
import { BottomNavigation, BottomNavigationTab, Icon } from '@ui-kitten/components';
import { OrderProvider } from '../context/Order/OrderContext';
import DetailRekap from '../screens/order/DetailRekapScreen';
import OrderScreen from '../screens/order/OrderScreen';
import { ReportProvider } from '../context/Report/ReportContext';
import TokoScreen from '../screens/home/toko/TokoScreen';
import SettingScreen from '../screens/setting/SettingScreen';
import { ShopProvider } from '../context/Shop/ShopContext';
import { UsersContext } from '../context/Users/context';
import { DashboardProvider } from '../context/Dashboard/DashboardContext';
import FutsalScreen from '../screens/home/futsal/FutsalScreen';
import TenisScreen from '../screens/home/tenis/TenisScreen';
import GedungScreen from '../screens/home/gedung/GedungScreen';
import ClubhouseScreen from '../screens/home/clubhouse/ClubhouseScreen';
import LainnyaScreen from '../screens/home/lainnya/LainnyaScreen';
import { personIcon } from '../components/Icon';
import PesanScreen from '../screens/home/futsal/PesanScreen';
import BayarScreen from '../screens/home/futsal/BayarScreen';
import BerhasilScreen from '../screens/home/futsal/BerhasilScreen';
import { FutsalProvider } from '../context/Futsal/FutsalContext';
import PembayaranScreen from '../screens/home/pesanan/PembayaranScreen';
import BayarScreenToko from '../screens/home/toko/BayarScreen';
import { FoodcourtProvider } from '../context/Foodcourt/FoodcourtConsumer';
import FoodcourtScreen from '../screens/home/foodcourt/FoodcourtScreen';
import PesanTenisScreen from '../screens/home/tenis/PesanScreen';
import PeriksaGedungScreen from '../screens/home/gedung/PeriksaScreen';
import PaymentScreen from '../screens/home/PaymentScreen';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();
const HomeIcon = (props: any) => (
  <Icon {...props} name='home-outline' />
);

const ClipIcon = (props: any) => (
  <Icon {...props} name='clipboard-outline' />
);

const SettingIcon = (props: any) => (
  <Icon {...props} name='settings-outline' />
);

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();
  const user = React.useContext(UsersContext)
  return (
    <OrderProvider user={user}>
    <FoodcourtProvider user={user}>
    <ReportProvider user={user}>
    <ShopProvider user={user}>
    <DashboardProvider user={user}>
    <FutsalProvider user={user}>
    <BottomTab.Navigator
      initialRouteName="TabOne"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}
      tabBar={props => <BottomTabBar {...props} />}
    >
      <BottomTab.Screen
        name="TabOne"
        component={HomeNavigator}
      />

      <BottomTab.Screen
        name="Order"
        component={OrderNavigator}
      />

      <BottomTab.Screen
        name="Pesanan"
        component={SettingNavigator}
      />
    </BottomTab.Navigator>
    </FutsalProvider>
    </DashboardProvider>
    </ShopProvider>
    </ReportProvider>
    </FoodcourtProvider>
    </OrderProvider>
   
  );
}

const BottomTabBar = ({ state, navigation }: any) => {
  if (state != undefined) {
    let param = state.routes[state.index];
    if (param != undefined) {
      if (param.state != undefined) {
        param = param.state.routes[state.routes[state.index].state.index]
        if (param.params) {
          if (!param.params.tabBarVisibility) {
            return null
          }
        }
      }

    }

  }
  const [visible, setVisible] = React.useState(true)

  return visible ? (
    <BottomNavigation
      selectedIndex={state.index}
      onSelect={index => navigation.navigate(state.routeNames[index])}
      style={{ paddingBottom: 20 }}
    >
      <BottomNavigationTab icon={HomeIcon} title='HOME' />
      <BottomNavigationTab icon={ClipIcon} title='PESANAN' />
      <BottomNavigationTab icon={personIcon} title='AKUN' />
    </BottomNavigation>
  ) : (
    <>
    </>
  )
};

const HomeStack = createStackNavigator();
function HomeNavigator() {
  return (
    <HomeStack.Navigator >
      <HomeStack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ headerShown: false }}

      />
      <HomeStack.Screen
        name="Toko"
        component={TokoScreen}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Foodcourt"
        component={FoodcourtScreen}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Futsal"
        component={FutsalScreen}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Pesan"
        component={PesanScreen}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Bayar"
        component={BayarScreen}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Berhasil"
        component={BerhasilScreen}
        options={{ headerShown: false }}
      />
      <HomeStack.Screen
        name="Tenis"
        component={TenisScreen}
        options={{ headerShown: false }}
      />
       <HomeStack.Screen
        name="PesanTenis"
        component={PesanTenisScreen}
        options={{ headerShown: false }}
      />

      <HomeStack.Screen
        name="Gedung"
        component={GedungScreen}
        options={{ headerShown: false }}
      />

      <HomeStack.Screen
        name="PeriksaGedung"
        component={PeriksaGedungScreen}
        options={{ headerShown: false }}
      />

      <HomeStack.Screen
        name="Clubhouse"
        component={ClubhouseScreen}
        options={{ headerShown: false }}
      />

      <HomeStack.Screen
        name="Pembayaran"
        component={BayarScreenToko}
        options={{ headerShown: false }}
      />

      <HomeStack.Screen
        name="Payment"
        component={PaymentScreen}
        options={{ headerShown: false }}
      />

    </HomeStack.Navigator>
  );
}

const OrderStack = createStackNavigator();

function OrderNavigator() {
  return (
    <OrderStack.Navigator>
      <OrderStack.Screen
        name="Order"
        component={OrderScreen}
        options={{ headerShown: false }}
      />

      <OrderStack.Screen
        name="DetailRekap"
        component={DetailRekap}
        options={{ headerShown: false }}
      />
    </OrderStack.Navigator>
  );
}


const SettingStack = createStackNavigator();

function SettingNavigator() {
  return (
    <SettingStack.Navigator>
      <SettingStack.Screen
        name="Setting"
        component={SettingScreen}
        options={{ headerShown: false }}
      />

    </SettingStack.Navigator>
  );
}
