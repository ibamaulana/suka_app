import { StatusBar } from 'expo-status-bar';
import React from 'react';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import * as eva from '@eva-design/eva';
import { ApplicationProvider,IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { default as theme } from './theme.json';
import Toast from 'react-native-toast-message';
import * as SecureStore from 'expo-secure-store';
import LoginScreen from './screens/LoginScreen';
import { UserContext } from './context/Auth/context';
import { UserProvider } from './context/Auth/UserContext';
import MainScreen from './screens/MainScreen';
import { UsersContext } from './context/Users/context';
import { UsersProvider } from './context/Users/UserContext';

export default function App() {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider {...eva} theme={{ ...eva.light, ...theme }}>
          <UsersProvider>
            <MainScreen />
          </UsersProvider>
          <StatusBar style="dark" />
          <Toast ref={(ref) => Toast.setRef(ref)} />
        </ApplicationProvider>
      </>
    );
  }
}
