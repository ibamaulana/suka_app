import React from 'react';
import { StyleSheet, View} from 'react-native';
import { Select, SelectItem, IndexPath, Text, List, ListItem, CheckBox, Divider, Button } from '@ui-kitten/components';
import { FutsalContext } from '../context/Futsal/context';

export const FileUpload = (props:any) => {
  

  return(
    <>
    {
      props.label && (
        <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:20,alignItems:'flex-end'}}>
          <Text category="h5" style={{fontSize:18}}>{props.label}</Text>
        </View>
      )
    }
   
    <View style={{...styles.rowContainer,backgroundColor:'#fff',alignItems:'flex-end'}}>
      <Button  onPress={() => props.navigation.navigate('Bayar',{
      tabBarVisibility: false,
      })} style={{...styles.button,flex:1}} appearance="outline" status="basic" size='medium'>
          Ungguh File
      </Button>
    </View>
    </>
  )
}

const styles = StyleSheet.create({
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  button: {
    alignContent: 'flex-start',
    textAlign: 'left'
  },
})
