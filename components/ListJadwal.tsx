import React from 'react';
import { StyleSheet} from 'react-native';
import { Select, SelectItem, IndexPath, Text, List, ListItem, CheckBox, Divider } from '@ui-kitten/components';
import { FutsalContext } from '../context/Futsal/context';
import { UsersContext } from '../context/Users/context';
import Toast from 'react-native-toast-message';

export const ListJadwalFutsal = (props:any) => {
  const [data,setData] = React.useState(props.data)
  React.useEffect(() => {
    setData(props.data)
  },[props])
  const setChecked = (value:any,index:any) => {
    data[index].checked = value
    setData(data)
  }
  
  const renderChecker = (index:any) => {
    const [checked,setChecked] = React.useState(false)

    return (
    <CheckBox
      checked={checked}
      onChange={nextChecked => setChecked(nextChecked)}
      disabled={!data[index].available}
      >
      {``}
    </CheckBox>
    )
  }

  const renderItem = ({ item, index }:any) => (
    <ListItem
      title={`${item.title}`}
      description={() => <Text category="c1" status={item.available ? 'primary' : 'danger'} style={{marginLeft:7,marginTop:3}}>{item.description}</Text>}
      accessoryRight={() => renderChecker(index)}
      disabled
    />
  );

  return(
    <List
      style={styles.listcontainer}
      data={data}
      ItemSeparatorComponent={Divider}
      renderItem={renderItem}
      scrollEnabled={false}
    />
  )
}

export const ListJadwal = (props:any) => {
  const [data,setData] = React.useState([
    {
      title: '07.00 - 08.00',
      description: 'Sudah Terbooking',
      available: false,
      checked: false
    },
    {
      title: '08.00 - 09.00',
      description: 'Sudah Terbooking',
      available: false,
      checked: false
    },
    {
      title: '09.00 - 10.00',
      description: 'Sudah Terbooking',
      available: false,
      checked: false
    },
    {
      title: '10.00 - 11.00',
      description: 'Sudah Terbooking',
      available: false,
      checked: false
    },
    {
      title: '11.00 - 12.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    },
    {
      title: '12.00 - 13.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    },
    {
      title: '13.00 - 14.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    },
    {
      title: '14.00 - 15.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    },
    {
      title: '16.00 - 17.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    },
    {
      title: '18.00 - 19.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    },
    {
      title: '19.00 - 20.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    },
    {
      title: '20.00 - 21.00',
      description: 'Lapangan Tersedia',
      available: true,
      checked: false
    }
  ])
  const setChecked = (value:any,index:any) => {
    console.log(index,value)
    data[index].checked = value
    setData(data)
  }
  
  const renderChecker = (index:any) => {
    const [checked,setChecked] = React.useState(false)

    return (
    <CheckBox
      checked={checked}
      onChange={nextChecked => setChecked(nextChecked)}
      disabled={!data[index].available}
      >
      {``}
    </CheckBox>
    )
  }

  const renderItem = ({ item, index }:any) => (
    <ListItem
      title={`${item.title}`}
      description={() => <Text category="c1" status={item.available ? 'primary' : 'danger'} style={{marginLeft:7,marginTop:3}}>{item.description}</Text>}
      accessoryRight={() => renderChecker(index)}
      disabled
    />
  );

  return(
    <List
      style={styles.listcontainer}
      data={data}
      ItemSeparatorComponent={Divider}
      renderItem={renderItem}
      scrollEnabled={false}
    />
  )
}

const styles = StyleSheet.create({
  listcontainer: {
    minHeight: 500,
    paddingHorizontal:20,
    backgroundColor:'#f7f7f7'
  },
})
