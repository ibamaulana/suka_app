import React from 'react';
import { Image,StyleSheet } from 'react-native';
import { Card,Text, Layout, RangeDatepicker,Select,SelectItem,IndexPath, Datepicker  } from '@ui-kitten/components';
import { OrderContext } from '../context/Order/context';
import {  View } from '../components/Themed';
import { homeIcon,formIcon,formatNumber, calendarIcon } from '../components/Icon';
import { ReportContext } from '../context/Report/context';
import Carousel from 'react-native-snap-carousel';
import { UsersContext } from '../context/Users/context';
import { SelectForm } from './SelectForm';

export const CardHeader = (props:any) => {
  const report = React.useContext(ReportContext)
  const user = React.useContext(UsersContext)

  return(
    <Layout style={styles.containerLayout} level='1'>
      <View style={styles.cardHeaderTop}>
        <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
          <Text category="h5" style={styles.shop}>SIMANIS - IF SEMARANG</Text>
        </View>
      </View>
    </Layout>
  )
}


export const HomeCarousel = () => {
    const item = [
      {
        title: 'Total Penjualan',
        value: 150000,
        percent: 20
      },
      {
        title: 'Total Diskon',
        value: 150000,
        percent: -10
      },
      {
        title: 'HPP',
        value: 150000,
        percent: 20
      },
      {
        title: 'Total Pajak',
        value: 150000,
        percent: 10
      },
      {
        title: 'Total Servis',
        value: 150000,
        percent: 10
      },
      {
        title: 'Laba Kotor',
        value: 150000,
        percent: 10
      },
      
    ];
    const renderItem = ({item, index}:any) => {
        return (
          <Card  appearance="filled" style={styles.card}>
            <Image source={require('../assets/icon/kampus.png')} style={styles.icon} /> 
          </Card>
        );
    }

    return (
    <Layout style={styles.carouselLayout} level='1'>
        <Carousel
          layout={"default"}
          data={item}
          renderItem={renderItem}
          sliderWidth={330}
          itemWidth={330}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          enableMomentum={true}
          activeSlideAlignment={'start'}
          style={{
            marginBottom:50
          }}
        />
       </Layout>
    );
}

export const FormFilter = () => {
  const order = React.useContext(OrderContext)
  const [date, setDate] = React.useState(new Date());
  const [lapangan, setLapangan] =React.useState(1);
  const lapanganList = [
    {
      label:'Lapangan 1',
      value:1,
    },
    {
      label:'Lapangan 2',
      value:2,
    },
    {
      label:'Lapangan 3',
      value:3,
    },
  ];
  return(
    <>
    
    	<Layout style={styles.inputContainer} level="1">
        <Datepicker
          label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
          date={date}
          onSelect={nextDate => setDate(nextDate)}
          style={styles.input}
          placeholder='Tanggal'
        />
      </Layout>
      <View style={styles.inputContainer}>
        <SelectForm
          value_list={lapanganList}
          label="Lapangan"
          value={lapangan}
          setValue={(value:any) => setLapangan(value)}
          
        />
      </View>
     
     
      
    </>
  )
}


const styles = StyleSheet.create({
  card:{
    height:250,
    padding:0,
    marginRight:10,
    backgroundColor: 'transparent',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left',
    color: 'black'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
    
  },
  shop:{
    flex:1,
    textAlign:'center',
    // marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  icon:{ 
    width: 350,
    height: 250,
    resizeMode:'contain',
    alignSelf:'center',
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  carouselLayout: {
    marginTop:-100,
    marginLeft:20,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor:'transparent'
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-50,
    marginBottom:20,
    padding:20,
    backgroundColor: 'transparent',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  carousel: {
    flex:1,
    marginBottom:20,
    padding:20,
    // backgroundColor: 'transparent',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },

  menu: {
    marginTop:20,
    backgroundColor: '#ECF5FF',
    borderWidth:0,
    borderRadius:10,
    height:95,
    width:95,
    justifyContent:'center'
  },
});