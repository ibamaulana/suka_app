import React from 'react';
import { Image,ScrollView,StyleSheet } from 'react-native';
import { Card,Text, Layout, RangeDatepicker,Select,SelectItem,IndexPath, Datepicker, Button, Divider  } from '@ui-kitten/components';
import { OrderContext } from '../context/Order/context';
import {  View } from '../components/Themed';
import { homeIcon,formIcon,formatNumber, calendarIcon } from '../components/Icon';
import { ReportContext } from '../context/Report/context';
import Carousel from 'react-native-snap-carousel';
import { UsersContext } from '../context/Users/context';
import { SelectForm } from './SelectForm';
import { FutsalContext } from '../context/Futsal/context';
import { FileUpload } from './FileUpload';
import Toast from 'react-native-toast-message';
import { ListJadwalFutsal } from './ListJadwal';
import moment from 'moment';

export const FormFilterFutsal = (props:any) => {
  const users = React.useContext(UsersContext);
  const futsal = React.useContext(FutsalContext)
  const getJadwal = async (param?: any) =>{
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/order/jadwal/futsal?' + new URLSearchParams(param).toString(), {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+users.state.token
          },
        }
      );
      let json = await response.json();
      setJadwal(json.data)
    } catch (error) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: 'yang itu',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  React.useEffect(() => {
    let start_date = new Date()
    let end_date = new Date()
    
    if(futsal.state.layanan === 'reguler'){
      start_date = futsal.state.date
      end_date = futsal.state.date
    }else{
      start_date = futsal.state.range_date.startDate
      end_date = futsal.state.range_date.endDate
    }
    let param = {
      start_date: moment(start_date).format('YYYY-MM-DD'),
      end_date: moment(end_date).format('YYYY-MM-DD'),
    }
    getJadwal(param)
  },[futsal.state])

  const [jadwal,setJadwal] = React.useState({
    'jadwal' : []
  })
  return(
    <>
      {
        futsal.state.layanan === 'turnamen' ? (
          <Layout style={styles.inputContainer} level="1">
            <RangeDatepicker
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
              range={futsal.state.range_date}
              onSelect={nextRange => futsal.dispatch({type:'update-field',field:'range_date',value:nextRange})}
              style={styles.input}
            />
          </Layout>
        ) : (
          <Layout style={styles.inputContainer} level="1">
            <Datepicker
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
              date={futsal.state.date}
              onSelect={nextDate => futsal.dispatch({type:'update-field',field:'date',value:nextDate})}
              style={styles.input}
              placeholder='Tanggal'
            />
          </Layout>
        )}
      <ScrollView style={{width:'100%',height:100,marginTop:20}}>
        {
          futsal.state.layanan === 'turnamen' ? (
            <>
            <FileUpload
              label="Unggah Dokumen Proposal Turnamen"
            />
            <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:40,alignItems:'flex-end',height:130}}>
                <Button onPress={() => props.navigation.navigate('Bayar',{
                tabBarVisibility: false,
              })} style={{...styles.button,flex:1}} size='giant'>
                  Pesan
                </Button>
              </View>
            </>
        ) : (
          <>
            <Layout style={styles.tabContainer}>
              {
                Object.keys(jadwal).map((val,index) => 
                  <>
                  <View style={{...styles.rowContainer,backgroundColor:'#f7f7f7',paddingVertical:20}}>
                    <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
                    {val}</Text>
                  </View>
                  <ListJadwalFutsal
                    data={jadwal[val]}
                  />
                  </>
                )
              }
              
            </Layout>
            <View style={{...styles.rowContainer,backgroundColor:'#f7f7f7',paddingBottom:40,alignItems:'flex-end',height:130}}>
              <Button onPress={() => props.navigation.navigate('Bayar',{
              tabBarVisibility: false,
            })} style={{...styles.button,flex:1}} size='giant'>
                Pesan
              </Button>
            </View>
          </>

          )
        }
          
      </ScrollView>
    </>
  )
}

export const FormFilterTenis = (props:any) => {
  const users = React.useContext(UsersContext);
  const futsal = React.useContext(FutsalContext)
  const getJadwal = async (param?: any) =>{
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/order/jadwal/futsal?' + new URLSearchParams(param).toString(), {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+users.state.token
          },
        }
      );
      let json = await response.json();
      setJadwal(json.data)
    } catch (error) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: 'yang itu',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  React.useEffect(() => {
    let start_date = new Date()
    let end_date = new Date()
    
    if(futsal.state.layanan === 'reguler'){
      start_date = futsal.state.date
      end_date = futsal.state.date
    }else{
      start_date = futsal.state.range_date.startDate
      end_date = futsal.state.range_date.endDate
    }
    let param = {
      start_date: moment(start_date).format('YYYY-MM-DD'),
      end_date: moment(end_date).format('YYYY-MM-DD'),
    }
    getJadwal(param)
  },[futsal.state])

  const [jadwal,setJadwal] = React.useState({
    'jadwal' : []
  })

  const [durasi,setDurasi] = React.useState(6)

  const durasiList = [
    {
      label:'6 Jam',
      value:6,
    },
    {
      label:'7 Jam',
      value:7,
    },
    {
      label:'8 Jam',
      value:8,
    },
    {
      label:'9 Jam',
      value:9,
    },
  ];

  const [jammulai,setjammulai] = React.useState(1)

  const jammulaiList = [
    {
      label:'08:00',
      value:1,
    },
    {
      label:'09:00',
      value:2,
    },
    {
      label:'10:00',
      value:3,
    },
    {
      label:'11:00',
      value:4,
    },
  ];
  return(
    <>
      {
        futsal.state.layanan === 'turnamen' ? (
          <>
          <Layout style={styles.inputContainer} level="1">
            <RangeDatepicker
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
              range={futsal.state.range_date}
              onSelect={nextRange => futsal.dispatch({type:'update-field',field:'range_date',value:nextRange})}
              style={styles.input}
            />
          </Layout>
          <Layout style={styles.inputContainer} level="1">
            <SelectForm
              value_list={durasiList}
              label="Durasi"
              value={durasi}
              setValue={(value:any) => setDurasi(value)}
            />
          </Layout>
          <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:5,alignItems:'flex-end'}}>
            <Button status='warning' onPress={() => props.navigation.navigate('Bayar',{
            tabBarVisibility: false,
          })} style={{...styles.button,flex:1}} size='giant'>
              Periksa Ketersediaan
            </Button>
          </View>
          </>
        ) : (
          <Layout style={styles.inputContainer} level="1">
            <Datepicker
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
              date={futsal.state.date}
              onSelect={nextDate => futsal.dispatch({type:'update-field',field:'date',value:nextDate})}
              style={styles.input}
              placeholder='Tanggal'
            />
          </Layout>
        )}
        {
          futsal.state.layanan === 'turnamen' ? (
          <ScrollView style={{width:'100%',height:100,marginTop:20,backgroundColor:'#f7f7f7'}}>
           <View style={{backgroundColor:'#fff',marginHorizontal:20,paddingVertical:10,marginTop:10}}>
            <View style={{...styles.rowContainer,backgroundColor:'#fff',marginHorizontal:0,paddingVertical:0,marginTop:10,marginBottom:10}}>
              <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
              2021-12-15</Text>
              <Text category="s2" status="success" style={{flex:1,textAlign:'right',fontSize:17}}>
              Tersedia</Text>
            </View>
            <Divider/>
            <View style={{...styles.rowContainer,backgroundColor:'#fff',marginTop:10,marginBottom:10}}>
            <SelectForm
              value_list={jammulaiList}
              label="Jam Mulai"
              value={jammulai}
              setValue={(value:any) => setjammulai(value)}
            />
            <SelectForm
              value_list={jammulaiList}
              label="Jam Selesai"
              value={jammulai}
              setValue={(value:any) => setjammulai(value)}
            />
            </View>
            </View>
            <View style={{backgroundColor:'#fff',marginHorizontal:20,paddingVertical:10,marginTop:10}}>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',marginHorizontal:0,paddingVertical:0,marginTop:10,marginBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
                2021-12-16</Text>
                <Text category="s2" status="danger" style={{flex:1,textAlign:'right',fontSize:17}}>
                Tidak Tersedia</Text>
              </View>
            </View>
            <View style={{backgroundColor:'#fff',marginHorizontal:20,paddingVertical:10,marginTop:10}}>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',marginHorizontal:0,paddingVertical:0,marginTop:10,marginBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
                2021-12-15</Text>
                <Text category="s2" status="success" style={{flex:1,textAlign:'right',fontSize:17}}>
                Tersedia</Text>
              </View>
              <Divider/>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',marginTop:10,marginBottom:10}}>
              <SelectForm
                value_list={jammulaiList}
                label="Jam Mulai"
                value={jammulai}
                setValue={(value:any) => setjammulai(value)}
              />
              <SelectForm
                value_list={jammulaiList}
                label="Jam Selesai"
                value={jammulai}
                setValue={(value:any) => setjammulai(value)}
              />
              </View>
            </View>
            <View style={{...styles.rowContainer,backgroundColor:'#f7f7f7',paddingBottom:40,alignItems:'flex-end',height:130}}>
                <Button onPress={() => props.navigation.navigate('Bayar',{
                tabBarVisibility: false,
              })} style={{...styles.button,flex:1}} size='giant'>
                  Pesan
                </Button>
            </View>
          </ScrollView>
        ) : (
          <ScrollView style={{width:'100%',height:100,marginTop:20}}>

            <Layout style={styles.tabContainer}>
              {
                Object.keys(jadwal).map((val,index) => 
                  <>
                  <View style={{...styles.rowContainer,backgroundColor:'#f7f7f7',paddingVertical:20}}>
                    <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
                    {val}</Text>
                  </View>
                  <ListJadwalFutsal
                    data={jadwal[val]}
                  />
                  </>
                )
              }
              
            </Layout>
            <View style={{...styles.rowContainer,backgroundColor:'#f7f7f7',paddingBottom:40,alignItems:'flex-end',height:130}}>
              <Button onPress={() => props.navigation.navigate('Bayar',{
              tabBarVisibility: false,
            })} style={{...styles.button,flex:1}} size='giant'>
                Pesan
              </Button>
            </View>
          </ScrollView>

          )
        }
    </>
  )
}

export const FormFilterGedung = (props:any) => {
  const users = React.useContext(UsersContext);
  const futsal = React.useContext(FutsalContext)
  const getJadwal = async (param?: any) =>{
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/order/jadwal/futsal?' + new URLSearchParams(param).toString(), {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+users.state.token
          },
        }
      );
      let json = await response.json();
      setJadwal(json.data)
    } catch (error) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: 'yang itu',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  React.useEffect(() => {
    let start_date = new Date()
    let end_date = new Date()
    
    if(futsal.state.layanan === 'reguler'){
      start_date = futsal.state.date
      end_date = futsal.state.date
    }else{
      start_date = futsal.state.range_date.startDate
      end_date = futsal.state.range_date.endDate
    }
    let param = {
      start_date: moment(start_date).format('YYYY-MM-DD'),
      end_date: moment(end_date).format('YYYY-MM-DD'),
    }
    getJadwal(param)
  },[futsal.state])

  const [jadwal,setJadwal] = React.useState({
    'jadwal' : []
  })

  const [durasi,setDurasi] = React.useState(6)

  const durasiList = [
    {
      label:'6 Jam',
      value:6,
    },
    {
      label:'7 Jam',
      value:7,
    },
    {
      label:'8 Jam',
      value:8,
    },
    {
      label:'9 Jam',
      value:9,
    },
  ];

  const [jammulai,setjammulai] = React.useState(1)

  const jammulaiList = [
    {
      label:'08:00',
      value:1,
    },
    {
      label:'09:00',
      value:2,
    },
    {
      label:'10:00',
      value:3,
    },
    {
      label:'11:00',
      value:4,
    },
  ];

  const [layanan,setLayanan] = React.useState('umum')

  const layananList = [
    {
      label:'Umum',
      value:'umum',
    },
    {
      label:'Alumni Mahasiswa UIN',
      value:'alumni',
    },
    {
      label:'Pensiunan UIN Suka',
      value:'pensiunan',
    },
  ];

  const [jamacara,setJamacara] = React.useState('siang')

  const jamacaraList = [
    {
      label:'Siang Hari',
      value:'siang',
    },
    {
      label:'Malam Hari',
      value:'malam',
    },
  ];
  return(
    <>
      <Layout style={styles.inputContainer} level="1">
        <SelectForm
          value_list={layananList}
          label="Status Layanan"
          value={layanan}
          setValue={(value:any) => setLayanan(value)}
        />
      </Layout>
      <Layout style={styles.inputContainer} level="1">
        <Datepicker
          label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
          date={futsal.state.date}
          onSelect={nextRange => futsal.dispatch({type:'update-field',field:'range_date',value:nextRange})}
          style={styles.input}
        />
        <SelectForm
          value_list={jamacaraList}
          label="Jam Acara"
          value={jamacara}
          setValue={(value:any) => setJamacara(value)}
        />
      </Layout>
      
          <View style={{...styles.rowContainer,backgroundColor:'#fff',paddingBottom:5,alignItems:'flex-end'}}>
            <Button status='warning' onPress={() => props.navigation.navigate('Bayar',{
            tabBarVisibility: false,
          })} style={{...styles.button,flex:1}} size='giant'>
              Periksa Ketersediaan
            </Button>
          </View>
          <ScrollView style={{width:'100%',height:100,marginTop:20,backgroundColor:'#f7f7f7'}}>
           <View style={{backgroundColor:'#fff',marginHorizontal:20,paddingVertical:10,marginTop:10}}>
            <View style={{...styles.rowContainer,backgroundColor:'#fff',marginHorizontal:0,paddingVertical:0,marginTop:10,marginBottom:10}}>
              <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
              2021-12-15</Text>
              <Text category="s2" status="success" style={{flex:1,textAlign:'right',fontSize:17}}>
              Tersedia</Text>
            </View>
            <Divider/>
            <View style={{...styles.rowContainer,backgroundColor:'#fff',marginTop:10,marginBottom:10}}>
            <SelectForm
              value_list={jammulaiList}
              label="Jam Mulai"
              value={jammulai}
              setValue={(value:any) => setjammulai(value)}
            />
            <SelectForm
              value_list={jammulaiList}
              label="Jam Selesai"
              value={jammulai}
              setValue={(value:any) => setjammulai(value)}
            />
            </View>
            </View>
            <View style={{backgroundColor:'#fff',marginHorizontal:20,paddingVertical:10,marginTop:10}}>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',marginHorizontal:0,paddingVertical:0,marginTop:10,marginBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
                2021-12-16</Text>
                <Text category="s2" status="danger" style={{flex:1,textAlign:'right',fontSize:17}}>
                Tidak Tersedia</Text>
              </View>
            </View>
            <View style={{backgroundColor:'#fff',marginHorizontal:20,paddingVertical:10,marginTop:10}}>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',marginHorizontal:0,paddingVertical:0,marginTop:10,marginBottom:10}}>
                <Text category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
                2021-12-15</Text>
                <Text category="s2" status="success" style={{flex:1,textAlign:'right',fontSize:17}}>
                Tersedia</Text>
              </View>
              <Divider/>
              <View style={{...styles.rowContainer,backgroundColor:'#fff',marginTop:10,marginBottom:10}}>
              <SelectForm
                value_list={jammulaiList}
                label="Jam Mulai"
                value={jammulai}
                setValue={(value:any) => setjammulai(value)}
              />
              <SelectForm
                value_list={jammulaiList}
                label="Jam Selesai"
                value={jammulai}
                setValue={(value:any) => setjammulai(value)}
              />
              </View>
            </View>
            <View style={{...styles.rowContainer,backgroundColor:'#f7f7f7',paddingBottom:40,alignItems:'flex-end',height:130}}>
                <Button onPress={() => props.navigation.navigate('Bayar',{
                tabBarVisibility: false,
              })} style={{...styles.button,flex:1}} size='giant'>
                  Pesan
                </Button>
            </View>
          </ScrollView>
       
    </>
  )
}

export const FormFilter = () => {
  const futsal = React.useContext(FutsalContext)
  const [date, setDate] = React.useState(new Date());
  const [range, setRange] = React.useState({});
  const [lapangan, setLapangan] =React.useState(1);
  const lapanganList = [
    {
      label:'Lapangan 1',
      value:1,
    },
    {
      label:'Lapangan 2',
      value:2,
    },
    {
      label:'Lapangan 3',
      value:3,
    },
  ];
  return(
    <>
    	
      {
        futsal.state.layanan === 'turnamen' ? (
          <Layout style={styles.inputContainer} level="1">
            <RangeDatepicker
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
              range={range}
              onSelect={nextRange => setRange(nextRange)}
              style={styles.input}
            />
          </Layout>
        ) : (
          <Layout style={styles.inputContainer} level="1">
            <Datepicker
              label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>Tanggal</Text>}
              date={date}
              onSelect={nextDate => setDate(nextDate)}
              style={styles.input}
              placeholder='Tanggal'
            />
          </Layout>
        )}
      {
        futsal.state.order_type === 'tenis' && (
        <View style={styles.inputContainer}>
          <SelectForm
            value_list={lapanganList}
            label="Lapangan"
            value={lapangan}
            setValue={(value:any) => setLapangan(value)}
            
          />
        </View>
        )
      }
      
    </>
  )
}


const styles = StyleSheet.create({
  tabContainer: {
    height: 'auto',
    backgroundColor:'#f7f7f7'
  },
  card:{
    height:250,
    padding:0,
    marginRight:10,
    backgroundColor: 'transparent',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left',
    color: 'black'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
    
  },
  shop:{
    flex:1,
    textAlign:'center',
    // marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  icon:{ 
    width: 350,
    height: 250,
    resizeMode:'contain',
    alignSelf:'center',
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  carouselLayout: {
    marginTop:-100,
    marginLeft:20,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor:'transparent'
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-50,
    marginBottom:20,
    padding:20,
    backgroundColor: 'transparent',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  carousel: {
    flex:1,
    marginBottom:20,
    padding:20,
    // backgroundColor: 'transparent',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },

  menu: {
    marginTop:20,
    backgroundColor: '#ECF5FF',
    borderWidth:0,
    borderRadius:10,
    height:95,
    width:95,
    justifyContent:'center'
  },
});