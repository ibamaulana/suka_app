import React from 'react';
import { StyleSheet,KeyboardAvoidingView,Animated,Dimensions,Platform } from 'react-native';
import { Input,Button,Text, Layout, RangeDatepicker,Select,SelectItem,IndexPath  } from '@ui-kitten/components';
import { OrderContext } from '../context/Order/context';
import {  View } from '../components/Themed';
import { personIcon,homeIcon,formIcon,formatNumber, plusIcon, percentIcon } from '../components/Icon';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { UserContext } from '../context/Auth/context';
import Toast from 'react-native-toast-message';
import CurrencyInput from 'react-native-currency-input';
import { ReportContext } from '../context/Report/context';
import DateTimePicker from '@react-native-community/datetimepicker';
import { ShopContext } from '../context/Shop/context';
import { env } from '../config/environtment';

export const CardHeader = (props:any) => {
  const report = React.useContext(ReportContext)
  const user = React.useContext(UserContext)
  React.useEffect(() => {
    const dashboardAsync = async (token:any) =>{
      try {
        let response = await fetch(
          `${env.API_URL}/v2/report/dashboard?outlet_id=${user.data.user.outlet_id}`, {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        if(json.status_code != 200){
          Toast.show({
            type: 'error',
            position: 'top',
            text1: 'Error',
            text2: json.message,
            visibilityTime: 4000,
            autoHide: true,
            topOffset: 80,
            bottomOffset: 80,
          });
        }else{
          report.dispatch({type:'update-dashboard',dashboard:json.data})
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: error.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    dashboardAsync(user.data.token)
  },[])
  return(
    <Layout style={styles.containerLayout} level='1'>
      <View style={styles.cardHeaderTop}>
        <View style={{flexDirection:'row',backgroundColor:'#fff',borderBottomColor: '#ECF2FF',borderBottomWidth:2,marginBottom:20}}>
          <Text category="h5" style={styles.shop}>{user.data.shop.shop_name}</Text>
        </View>
        <View style={{flexDirection:'row'}}>
          <View style={{flex:1,backgroundColor:'#fff'}}>
            <Text category="s1" style={styles.headerLeft}>Pemasukan </Text>
            <Text category="h5" style={styles.headerLeft}>{formatNumber(report.state.dashboard.totalgross)}</Text>
          </View>
          <View style={{flex:1,backgroundColor:'#fff'}}>
            <Text category="s1" style={{textAlign:'right',color: '#01573c'}}>Transaksi</Text>
            <Text category="h5" style={{textAlign:'right',color: '#01573c'}}>{report.state.dashboard.totaltransaction}</Text>
          </View>
        </View>
        <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
          <Button appearance='filled'  onPress={() => props.navigation.push('Pesanan',{
          tabBarVisibility: false,
        })} style={styles.button} status='primary' accessoryLeft={plusIcon} >
            Buat Pesanan
          </Button>
        </View>
      </View>
    </Layout>
  )
}

export const FormShop = (props:any) => {
  const shop = React.useContext(ShopContext)
  const [range, setRange] = React.useState({});
  React.useEffect(() => {
    const shopAsync = async (token:any) =>{
      try {
        let response = await fetch(
          `${env.API_URL}/shop?shop_id=${props.user.data.user.shop_id}`, {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        if(json.status_code != 200){
          Toast.show({
            type: 'error',
            position: 'top',
            text1: 'Error',
            text2: json.message,
            visibilityTime: 4000,
            autoHide: true,
            topOffset: 80,
            bottomOffset: 80,
          });
        }else{
          shop.dispatch({type:'update_shop',data:json.data})
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: error.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    shopAsync(props.user.data.token);
  },[])
  //tax
  const [viewIndex, setViewIndex] = React.useState<any>(new IndexPath(0));
  const view_list = [
    'Ada',
    'Tidak Ada',
  ];
  const view = view_list[viewIndex.row];

  const [outletIndex, setOutletIndex] = React.useState<any>(new IndexPath(0));
  const outlet_list = [
    'Outlet 1',
    'Outlet 2',
    'Outlet 3',
  ];
  const outlet = outlet_list[outletIndex.row];
 
  const renderOption = (title:any) => (
    <SelectItem  title={title}/>
  );
  return(
    <>
      <View style={styles.inputContainer}>
        <Input
          label='Nama Toko'
          placeholder='Nama Toko'
          value={shop.state.shop_name}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_name',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Email Toko'
          placeholder='Email Toko'
          value={shop.state.shop_email}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_email',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Alamat Toko'
          placeholder='Alamat Toko'
          value={shop.state.shop_address}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_address',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Instagram'
          placeholder='Instagram'
          value={shop.state.shop_instagram}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_instagram',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Facebook'
          placeholder='Facebook'
          value={shop.state.shop_facebook}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_facebook',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Website'
          placeholder='Website'
          value={shop.state.shop_web}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_web',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
      <Select
          label="Tax"
          style={styles.input}
          value={view}
          selectedIndex={viewIndex}
          onSelect={(index:any) => setViewIndex(index)}
          >
          {view_list.map(renderOption)}
        </Select>
        <Input
          label='Service'
          placeholder='service'
          value={shop.state.service}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'service',
              value:nextValue
            })}
          accessoryRight={percentIcon}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Select
          label="Tipe Tax"
          style={styles.input}
          value={view}
          selectedIndex={viewIndex}
          onSelect={(index:any) => setViewIndex(index)}
          >
          {view_list.map(renderOption)}
        </Select>
        <Select
          label="Tipe Service"
          style={styles.input}
          value={view}
          selectedIndex={viewIndex}
          onSelect={(index:any) => setViewIndex(index)}
          >
          {view_list.map(renderOption)}
        </Select>
      </View>
      <Button status="primary" style={styles.payButton} size="giant"  appearance='filled'   >
        Simpan
      </Button>
      {/* <View style={styles.inputContainer}>
      <Select
          style={styles.input}
          value={view}
          selectedIndex={viewIndex}
          onSelect={(index:any) => setViewIndex(index)}
          accessoryLeft={formIcon}
          >
          {view_list.map(renderOption)}
        </Select>
      </View>
    	<Layout style={styles.inputContainer} level="1">
        <RangeDatepicker
          range={range}
          onSelect={nextRange => setRange(nextRange)}
          style={styles.input}
          accessoryRight={calendarIcon}
        />
      </Layout> */}
     
     
      
    </>
  )
}


const styles = StyleSheet.create({
  payButton:{
    width:'90%',
    backgroundColor:'#2BCC9C',
    borderColor: '#2BCC9C',
    alignSelf:'center',
    marginVertical:10
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:8,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
    
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  icon:{ 
    width: 50,
    height: 50,
    resizeMode:'contain',
    alignSelf:'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-50,
    marginBottom:20,
    padding:20,
    backgroundColor: '#fff',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },

  menu: {
    marginTop:20,
    backgroundColor: '#ECF5FF',
    borderWidth:0,
    borderRadius:10,
    height:95,
    width:95,
    justifyContent:'center'
  },
});