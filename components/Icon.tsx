import React from 'react';
import { Icon, Text } from '@ui-kitten/components';

export const totalIcon = (text:string) => {
  return(
    <Text style={{color:'black'}} category="s1">{text}</Text>
  )
}

export const plusIcon = (props:any) => (
  <Icon {...props} name='plus'/>
);

export const minusIcon = (props:any) => (
  <Icon {...props} name='minus'/>
);

export const arrowIcon = (props:any) => (
  <Icon {...props} name='arrow-ios-downward-outline'/>
);

export const ChevronDown = (props:any) => (
  <Icon {...props} name='chevron-down-outline'/>
);

export const ChevronUp = (props:any) => (
  <Icon {...props} name='chevron-up-outline'/>
);

export const searchIcon = (props:any) => (
  <Icon {...props} name='search'/>
);

export const forwardIcon = (props:any) => (
  <Icon {...props} name='arrow-ios-forward'/>
);

export const personIcon = (props:any) => (
  <Icon {...props} name='person-outline'/>
);

export const emailIcon = (props:any) => (
  <Icon {...props} name='email-outline'/>
);

export const phoneIcon = (props:any) => (
  <Icon {...props} name='phone-outline'/>
);

export const percentIcon = (props:any) => (
  <Icon {...props} name='percent-outline'/>
);

export const calendarIcon = (props:any) => (
  <Icon {...props} name='calendar-outline'/>
);

export const formIcon = (props:any) => (
  <Icon {...props} name='file-text-outline'/>
);

export const homeIcon = (props:any) => (
  <Icon {...props} name='home-outline'/>
);

export const drawerInfoIcon = (props:any) => (
  <Icon {...props} name='info'/>
);

export const drawerOutletIcon = (props:any) => (
  <Icon {...props} name='info'/>
);

export const ClockIcon = (props:any) => (
  <Icon {...props} name='clock-outline'/>
);

export const mapIcon = (props:any) => (
  <Icon {...props} name='map-outline'/>
);

export function formatNumber(number:any) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}