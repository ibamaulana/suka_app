import React from 'react';
import { StyleSheet,Dimensions } from 'react-native';
import { Layout, Spinner, Text } from '@ui-kitten/components';
import { View } from './Themed';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ChevronDown, ChevronUp } from './Icon';

export const LoadingScreen = () => {
  return(
    <Layout style={styles.container}>
       <Spinner status="primary" size='giant'/>
    </Layout>
  )
}

export const Toggle = (props:any) => {
  const [show,setShow] = React.useState(false)
  const toggle = () => {
    if(show){
      setShow(false)
    }else{
      setShow(true)
    }
  }
  return (
    <>
    <TouchableOpacity onPress={() => toggle()}>
    <View style={{...styles.rowContainer}}>
        <Text  category="h4" style={{flex:1,textAlign:'left',fontSize:17}}>
          {props.title}
        </Text>
        {
          show ? (
            <ChevronUp
            fill='#8F9BB3'
            style={styles.icon}
          />
          ) : (
            <ChevronDown
            fill='#8F9BB3'
            style={styles.icon}
          />
          )
        }
       
      </View>
    </TouchableOpacity>
      
      {
        show && (
          props.children
        )
      }
      
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
    justifyContent: 'center',
    height:'100%',
    width:'100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    zIndex:3,
    opacity:0.7,
    position:'absolute'
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal:20,
  },
  icon:{
    flex:1,
    width:20,
    height:20,
    color:'#000'
  }
});