import * as React from 'react';
import { Image,StyleSheet } from 'react-native';
import { Layout,Button,Card,Text  } from '@ui-kitten/components';
import { View } from '../components/Themed';
import { SelectForm } from '../components/SelectForm';
import { DashboardContext } from '../context/Dashboard/context';

export default function Dashboard(props?: any) {
  const dashboard = React.useContext(DashboardContext)
  //tax
  const bulan = [
    {
      label:'Januari',
      value:1,
    },
    {
      label:'Februari',
      value:2,
    },
    {
      label:'Maret',
      value:3,
    },
    {
      label:'April',
      value:4,
    },
    {
      label:'Mei',
      value:5,
    },
    {
      label:'Juni',
      value:6,
    },
    {
      label:'Juli',
      value:7,
    },
    {
      label:'Agustus',
      value:8,
    },
    {
      label:'September',
      value:9,
    },
    {
      label:'Oktober',
      value:10,
    },
    {
      label:'November',
      value:11,
    },
    {
      label:'Desember',
      value:12,
    },
  ];

  const tahun = [
    {
      label:'2021',
      value:2021,
    },
    {
      label:'2020',
      value:2020,
    },
    {
      label:'2019',
      value:2019,
    },
  ];

  return (
    <>
     <Layout style={{...styles.rowContainer,marginHorizontal:20}} level='1'>
        <SelectForm
          value_list={bulan}
          label=""
          value={dashboard.state.bulan}
          setValue={(value:any) => dashboard.dispatch({type:'update-field',field:'bulan',value:value})}
        >
        </SelectForm>

        <SelectForm
          value_list={tahun}
          label=""
          value={dashboard.state.tahun}
          setValue={(value:any) => dashboard.dispatch({type:'update-field',field:'tahun',value:value})}
        >
        </SelectForm>
      </Layout>
      <Layout style={{...styles.rowContainer,marginHorizontal:20}} level='1'>
        <Text category="p1" style={{textAlign:'left',fontWeight:'700',marginTop:20,flex:1}}>PENGAADAAN</Text>
      </Layout>
      <Layout style={{...styles.rowContainer,marginHorizontal:13,marginTop:20}} level='1'>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="success" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.pengadaan.diterima}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Diterima</Text>
            </View>
          </Card>
        </View>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="info" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.pengadaan.proses}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Proses</Text>
            </View>
          </Card>
        </View>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="danger" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.pengadaan.ditolak}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Ditolak</Text>
            </View>
          </Card>
        </View>
      </Layout>

      <Layout style={{...styles.rowContainer,marginHorizontal:20}} level='1'>
        <Text category="p1" style={{textAlign:'left',fontWeight:'700',marginTop:20,flex:1}}>PENGAADAAN PP</Text>
      </Layout>
      <Layout style={{...styles.rowContainer,marginHorizontal:13,marginTop:20}} level='1'>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="success" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.pengadaan_pp.diterima}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Diterima</Text>
            </View>
          </Card>
        </View>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="info" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.pengadaan_pp.proses}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Proses</Text>
            </View>
          </Card>
        </View>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="danger" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.pengadaan_pp.ditolak}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Ditolak</Text>
            </View>
          </Card>
        </View>
      </Layout>

      <Layout style={{...styles.rowContainer,marginHorizontal:20}} level='1'>
        <Text category="p1" style={{textAlign:'left',fontWeight:'700',marginTop:20,flex:1}}>PERMINTAAN</Text>
      </Layout>
      <Layout style={{...styles.rowContainer,marginHorizontal:13,marginTop:20}} level='1'>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="success" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.permintaan.diterima}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Diterima</Text>
            </View>
          </Card>
        </View>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="info" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.permintaan.proses}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Proses</Text>
            </View>
          </Card>
        </View>
        <View style={{flex:1,alignItems:'center'}}>
          <Card status="danger" style={styles.menu}>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="h1" style={{textAlign:'center',flex:1}}>{dashboard.state.data.permintaan.ditolak}</Text>
            </View>
            <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
              <Text category="p2" style={{textAlign:'center',fontWeight:'700',marginTop:13,flex:1}}>Ditolak</Text>
            </View>
          </Card>
        </View>
      </Layout>
      
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    
    
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  icon:{ 
    width: 50,
    height: 50,
    resizeMode:'contain',
    alignSelf:'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-50,
    marginBottom:20,
    padding:20,
    backgroundColor: '#fff',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },

  menu: {
    backgroundColor: '#fff',
    borderWidth:1,
    borderRadius:10,
    height:100,
    width:100,
    justifyContent:'center',
  },
});
