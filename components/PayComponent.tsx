import React from 'react';
import { StyleSheet,KeyboardAvoidingView,Animated,Dimensions,Platform } from 'react-native';
import { Input,Button,Text, Divider,Select,SelectItem,IndexPath  } from '@ui-kitten/components';
import { OrderContext } from '../context/Order/context';
import {  View } from '../components/Themed';
import { personIcon,emailIcon,phoneIcon,formatNumber } from '../components/Icon';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { UserContext } from '../context/Auth/context';
import Toast from 'react-native-toast-message';
import CurrencyInput from 'react-native-currency-input';

export const FormPelanggan = () => {
  const order = React.useContext(OrderContext)
  return React.useMemo(() => {
  return(
    <>
    	<View style={styles.inputContainer}>
        <Input
          placeholder='Nama pelanggan'
          value={order.state.customer_name}
          onChangeText={nextValue => order.dispatch({type:'customer_name',value:nextValue})}
          style={styles.input}
          accessoryLeft={personIcon}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          placeholder='Email'
          value={order.state.customer_email}
          onChangeText={nextValue => order.dispatch({type:'customer_email',value:nextValue})}
          style={styles.input}
          accessoryLeft={emailIcon}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          placeholder='Nomor pelanggan'
          value={order.state.customer_phone}
          onChangeText={nextValue => order.dispatch({type:'customer_phone',value:nextValue})}
          style={styles.input}
          accessoryLeft={phoneIcon}
        />
      </View>
    </>
  )
  },[order.state.customer_phone,order.state.customer_name,order.state.customer_email])
}

export const PaySummary = () => {
  const order = React.useContext(OrderContext)
  return React.useMemo(() => {
    return(
      <>
        <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
          <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Subtotal</Text>
          <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp {formatNumber(order.state.total)}</Text>
        </View>
        <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10}}>
          <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Diskon</Text>
          <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp 0</Text>
        </View>
        <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10,marginBottom:30}}>
          <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total</Text>
          <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp {formatNumber(order.state.total)}</Text>
        </View>
      </>
    )
  },[order.state.total])
}
export const FormPayment = (props:any) => {
  const order = React.useContext(OrderContext)
  const user = React.useContext(UserContext)
  const [typeIndex, setTypeIndex] = React.useState<any>(new IndexPath(0));
  const [edcIndex, setEdcIndex] = React.useState<any>(new IndexPath(0));
  const [ewalletIndex, setEwalletIndex] = React.useState<any>(new IndexPath(0));
  const [templatePay,setTemplatePay] = React.useState('')
  const payment_type = [
    'Cash',
    'EDC',
    'E-Wallet',
  ];
  const paymentType = payment_type[typeIndex.row];
  const ewallet = order.state.ewallet[ewalletIndex.row];
  const edc = order.state.edc[edcIndex.row];

  const renderOption = (title:any) => (
    <SelectItem key={title} title={title}/>
  );
  React.useEffect(() => {
    const edcAsync = async (token:any) =>{
      try {
        let response = await fetch(
          `https://backoffice.provey.id/api/edc?outlet_id=${user.data.user.outlet_id}`, {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        if(json.status_code != 200){
          Toast.show({
            type: 'error',
            position: 'top',
            text1: 'Error',
            text2: json.message,
            visibilityTime: 4000,
            autoHide: true,
            topOffset: 80,
            bottomOffset: 80,
          });
        }else{
          order.dispatch({type:'update_edc',edc:json.data})
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: error.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    const ewalletAsync = async (token:any) =>{
      try {
        let response = await fetch(
          `https://backoffice.provey.id/api/ewallet?outlet_id=${user.data.user.outlet_id}`, {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        if(json.status_code != 200){
          Toast.show({
            type: 'error',
            position: 'top',
            text1: 'Error',
            text2: json.message,
            visibilityTime: 4000,
            autoHide: true,
            topOffset: 80,
            bottomOffset: 80,
          });
        }else{
          order.dispatch({type:'update_ewallet',ewallet:json.data})
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: error.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    if(order.state.edc.length == 0){
      edcAsync(user.data.token);
    }
    if(order.state.ewallet.length == 0){
      ewalletAsync(user.data.token);
    }
  },[])
  const bayar = async () => {
    props.refs.current.hide()
    props.setLoading(true)
    let param = {
      cashier_id: user.data.user.id,
      total_price: order.state.total,
      total:order.state.total,
      payment_gateway:order.state.payment_type,
      payment_id:0,
      discount:order.state.discount,
      product_id:order.state.order,
      customer_name:order.state.customer_name,
      customer_email:order.state.customer_email,
      customer_phone:order.state.customer_phone,
    }
    try {
      let response = await fetch(
        `https://backoffice.provey.id/api/order/createretail`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+user.data.token
          },
          body: JSON.stringify(param)
        }
      );
      let json = await response.json();
      if(json.status_code != 200){
        props.setLoading(false)
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: json.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }else{
        props.setLoading(false)
        props.setModal(true)
        // order.dispatch({type:'update_edc',edc:json.data})
      }
    } catch (error) {
      props.setLoading(false)

      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: error.message,
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }
  return (
   <>
    <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10,alignItems:'center'}}>
      <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Media Pembayaran</Text>
      <Select
        style={styles.btn}
        value={paymentType}
        selectedIndex={typeIndex}
        onSelect={(index:any) => setTypeIndex(index)}>
        {payment_type.map(renderOption)}
      </Select>
    </View>
    {
      paymentType != 'Cash' && (
        <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10,alignItems:'center'}}>
          <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>{paymentType}</Text>
          <Select
            style={styles.btn}
            value={paymentType == 'EDC' ? edc : ewallet}
            selectedIndex={paymentType == 'EDC' ? edcIndex : ewalletIndex}
            onSelect={(index:any) => paymentType == 'EDC' ? setEdcIndex(index) : setEwalletIndex(index)}>
            {
              paymentType === 'EDC' ?
                order.state.edc.map((e:any) => renderOption(e.name)) :
                order.state.ewallet.map((e:any) => renderOption(e.name))
            }
          </Select>
        </View>
      )
    }
    
    <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:15,alignItems:'center'}}>
      <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Total</Text>
      <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp {formatNumber(order.state.total)}</Text>
    </View>
    <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10,alignItems:'center'}}>
      <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Uang Pelanggan</Text>
      {/* <Input
        style={styles.btn}
        placeholder='Active'
        keyboardType='number-pad'
        returnKeyType='done'
        >
      </Input> */}
      <CurrencyInput
        value={order.state.pay}
        onChangeValue={(value) => order.dispatch({type:'update_payment',
          total: order.state.total,
          discount: order.state.discount,
          pay: value,
          change: order.state.change,
          payment_type: order.state.payment_type,
          payment_id: order.state.payment_id,
          promo_id: order.state.promo_id,
        })}
        unit="Rp "
        delimiter=","
        separator="."
        precision={0}
        onChangeText={(formattedValue) => {
          console.log(formattedValue); // $2,310.46
        }}
        style={styles.btnpay}
      />
    </View>
    <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10,alignItems:'center'}}>
      <Button style={styles.pilihBtn} size="small" onPress={() => setTemplatePay('100k')} appearance='filled' status={templatePay == '100k' ? 'primary' : 'basic'}  >
          100 K
      </Button>
      <Button style={styles.pilihBtn} size="small" onPress={() => setTemplatePay('50k')} appearance='filled' status={templatePay == '50k' ? 'primary' : 'basic'} >
          50 K
      </Button>
      <Button style={styles.pilihBtn} size="small" onPress={() => setTemplatePay('20k')} appearance='filled' status={templatePay == '20k' ? 'primary' : 'basic'} >
          20 K
      </Button>
      <Button style={styles.pilihBtn} size="small" onPress={() => setTemplatePay('')} appearance='filled' status={templatePay == '' ? 'primary' : 'basic'} >
          Pas
      </Button>
    </View>
    <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:15,alignItems:'center'}}>
      <Text category="s1" style={{flex:1,textAlign:'left',color: '#01573c',fontWeight:'bold'}}>Kembalian</Text>
      <Text category="s2" style={{flex:1,textAlign:'right'}} status="danger">Rp {formatNumber(order.state.change)}</Text>
    </View>
    <View style={{flexDirection:'row',marginHorizontal:24,marginVertical:10,alignItems:'center',position:'absolute',bottom:15}}>
      <Button style={styles.payButton} size="giant" onPress={() => bayar()}  appearance='filled' status="success"  >
        Bayar
      </Button>
    </View>
    </>
 )
}
export const PayPanel = (props:any) => {
  const refs = props.refs
  const {height} = Dimensions.get('window')
  
  return(
    <>
        {/* <KeyboardAvoidingView behavior="position" style={{width:'100%',backgroundColor:'black'}}> */}
        {/* <KeyboardAvoidingView
     style={{width:'100%'}}
    behavior={Platform.OS === 'ios' ? 'position' : undefined}
    
  > */}
      <SlidingUpPanel ref={refs[1]}
          draggableRange={{top: height/1.2, bottom: 0}}
          showBackdrop={true}
          animatedValue={new Animated.Value(0)} 
        >

          <View  style={{...styles.subView,height: height/1.2}} >
            
          <Divider style={styles.dividers}></Divider>
          <FormPayment setLoading={props.setLoading} setModal={props.setModal} refs={refs[1]}></FormPayment>
            
        </View>

        </SlidingUpPanel>
        {/* </KeyboardAvoidingView> */}

    </>
  )
}

export const PromoPanel = (props:any) => {
  const order = React.useContext(OrderContext)
  const refs = props.refs
	const [selectedIndex, setSelectedIndex] = React.useState();

  return(
    <>
    	<SlidingUpPanel ref={refs[0]}
					draggableRange={{top: 500, bottom: 0}}
					showBackdrop={false}
          animatedValue={new Animated.Value(0)} 
				>
				{/* <KeyboardAvoidingView behavior="position" style={{width:'100%',zIndex:3}}> */}

        <View style={{...styles.subView,height: 500}} >
						
					<Divider style={styles.dividers}></Divider>
						
         </View>
         {/* </KeyboardAvoidingView> */}

        </SlidingUpPanel>
    </>
  )
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:5,
    marginHorizontal:20
  },
  input:{
    flex:1,
  },
  dividers:{
		width:'10%',
		height:5,
		marginBottom:10,
		alignSelf:'center'
  },
  subView: {
		bottom:0,
		borderTopLeftRadius:14,
		borderTopRightRadius:14,
		paddingTop:15,
  },
  payButton:{
    flex:1,
    alignSelf:'center',
    marginVertical:10
  },
  pilihBtn:{
		flex:1,
		marginHorizontal:5
	},
  btn:{
		alignSelf:'flex-end',
    width:150,
	},
  btnpay:{
		alignSelf:'flex-end',
    width:150,
    backgroundColor: '#f7f9fc',
    borderColor: '#e4e9f2',
    borderRadius: 4,
    borderWidth: 1,
    minHeight: 40,
    paddingVertical: 7,
    paddingHorizontal: 8,
    textAlign:'right'
	},
})