import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { Input,Layout,Button,Card,Text,Avatar, OverflowMenu, MenuItem  } from '@ui-kitten/components';
import { OrderContext } from '../context/Order/context';
import { ScrollView } from 'react-native-gesture-handler';
import {  View } from '../components/Themed';
import SkeletonContent from 'react-native-skeleton-content';
import { totalIcon,minusIcon,plusIcon,arrowIcon,searchIcon, formatNumber } from '../components/Icon';
import Toast from 'react-native-toast-message';

export const CategoryButton = () => {
  const button = () => {
      return(
      <Button style={styles.categoryButton} size="small" onPress={() => setVisible(true)} appearance='filled' status="basic" accessoryRight={arrowIcon}>
          Semua
      </Button>
  )};

  const [selectedIndex, setSelectedIndex] = React.useState<any>(0);
  const [visible, setVisible] = React.useState(false);

  const onItemSelect = (index:any) => {
    setSelectedIndex(index);
    setVisible(false);
  };
  return(
    <OverflowMenu
        anchor={button}
        visible={visible}
        selectedIndex={selectedIndex}
        onSelect={onItemSelect}
        onBackdropPress={() => setVisible(false)}>
        <MenuItem title='Users'/>
        <MenuItem title='Orders'/>
        <MenuItem title='Transactions'/>
    </OverflowMenu>
  )
}

export const ProductCard = ({prod,index}:any) => {
  const context = React.useContext(OrderContext)
  return React.useMemo(() => {
    return (
      <React.Fragment>
      <View style={{zIndex:1,width:'100%'}}>
        
      </View>
      <Card style={styles.card}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <View style={{backgroundColor:'#fff'}}>
          
          </View>
          <Image source={prod.image} style={styles.icon} /> 

          <View style={{flex:1,backgroundColor:'#fff',marginLeft:10}}>
            <Text category="s1" style={{textAlign:'left',color: '#111'}}>{prod.product_name}</Text>
            <View style={{flexDirection:'row',alignItems:'center',marginTop:5}}>
            <Text category="p1" style={{textAlign:'left',color: '#111'}}>{prod.product_description}</Text>
            </View>
            <View style={{flexDirection:'row',alignItems:'center',marginTop:5}}>
              <Text category="s2" style={{flex:1,textAlign:'left',color: '#01573c'}}>Rp {formatNumber(prod.final_price)}</Text>
            </View>
            <View style={{flexDirection:'row-reverse',alignItems:'center'}}>
              {
                prod.qty == 0 && (
                <Button style={{...styles.plusBadge,maxWidth:30}} disabled={prod.stock == 0} size="tiny" appearance='filled' status="success" accessoryLeft={plusIcon} 
                onPress={() => context.dispatch({
                  type:'addProduct',
                  products:prod,
                  index:index,
                  qty: prod.qty+1,
                  stock: prod.stock-1,
                  total: context.state.total+prod.final_price,
                  pay: context.state.total+prod.final_price,
                })}></Button>
              )}
              {
                prod.qty != 0 && (
                  <>
                  <Button style={{...styles.plusBadge,width:10}} disabled={prod.stock == 0} size="tiny" appearance='filled' status="success" accessoryLeft={plusIcon} 
                  onPress={() => context.dispatch({
                    type:'addProduct',
                    products:prod,
                    index:index,
                    qty: prod.qty+1,
                    stock: prod.stock-1,
                    total: context.state.total+prod.final_price,
                    pay: context.state.total+prod.final_price,
                  })}/>
                  <Button style={styles.totalBadge} size="tiny" appearance='filled' accessoryLeft={() => totalIcon(prod.qty)}/>

                  <Button style={styles.minusBadge} size="tiny" appearance='filled' accessoryLeft={minusIcon} onPress={() => context.dispatch({
                    type:'removeProduct',
                    products:prod,
                    index:index,
                    qty: prod.qty-1,
                    stock: prod.stock+1,
                    total: context.state.total-prod.final_price,
                    pay: context.state.total+prod.final_price,
                  })}/>
                  </>
                )}
            </View>
          </View>
        </View>
      </Card>
    </React.Fragment>
    );
  }, [context.state.product[index]]);
}

export const ProductLayout = (props:any) => {
  const context = React.useContext(OrderContext)
  const [product,setProduct] = React.useState([]);
  React.useEffect(() => {
    const productAsync = async (token:any) =>{
      try {
        // let response = await fetch(
        //   `https://backoffice.provey.id/api/category/product/all?outlet_id=1&shop_id=1`, {
        //     method: 'GET',
        //     headers: {
        //       Accept: 'application/json',
        //       'Content-Type': 'application/json',
        //       Authorization: 'Bearer '+token
        //     },
        //   }
        // );
        // let json = await response.json();
        // if(json.status_code != 200){
        //   Toast.show({
        //     type: 'error',
        //     position: 'top',
        //     text1: 'Error',
        //     text2: json.message,
        //     visibilityTime: 4000,
        //     autoHide: true,
        //     topOffset: 80,
        //     bottomOffset: 80,
        //   });
        // }else{
        //   json.data.map((e:any) => {
        //     e.qty = 0
        //   })
          
        // }
        const products = [
          {
            id: 1,
            product_name: 'Gelas - 240 ml',
            final_price: 50000,
            qty:0,
            stock:1000,
            image: require('../assets/icon/suka_water.jpg')
          },
          {
            id: 2,
            product_name: 'Botol - 600ml',
            final_price: 50000,
            qty:0,
            stock:1000,
            image: require('../assets/icon/suka_water.jpg')
          },
          {
            id: 3,
            product_name: 'Galon - 19L',
            final_price: 20000,
            qty:0,
            stock:1000,
            image: require('../assets/icon/suka_water.jpg')
          },
          {
            id: 4,
            product_name: 'Tumbler',
            final_price: 100000,
            qty:0,
            stock:1000,
            image: require('../assets/icon/tumbler.jpg')
          },
          {
            id: 5,
            product_name: 'Flashdisk - 16GB',
            final_price: 60000,
            qty:0,
            stock:1000,
            image: require('../assets/icon/flashdisk.jpg')
          },
          {
            id: 6,
            product_name: 'Payung',
            final_price: 120000,
            qty:0,
            stock:1000,
            image: require('../assets/icon/payung.jpg')
          },
          {
            id: 7,
            product_name: 'Masker',
            final_price: 50000,
            qty:0,
            stock:1000,
            image: require('../assets/icon/masker.jpg')
          }
        ]
        setProduct(products);
        context.dispatch({type:'update_product',product:products})
      } catch (error:any) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: error.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    // if(context.state.product.length == 0){
    //   productAsync(props.user.token);
    // }else{
    //   setProduct(context.state.product);
    // }
    productAsync(props.user.token);

  },[])
  return(
    <>
      <View style={styles.rowContainer}>
        <Input
          placeholder='Cari Produk'
          value={context.state.search}
          onChangeText={nextValue => context.dispatch({type:'searchProduct',value:nextValue})}
          style={styles.input}
          accessoryLeft={searchIcon}
        />
      </View>
      {
        product.length == 0 && (
          <View style={styles.rowContainer}>
            <SkeletonContent
                containerStyle={{flex: 1}}
                animationDirection="horizontalLeft"
                layout={[
                  { width: '100%', height: 80, marginBottom: 6 },
                  { width: '100%', height: 80, marginBottom: 6 },
                  { width: '100%', height: 80, marginBottom: 6 },
                  { width: '100%', height: 80, marginBottom: 6 },
                  { width: '100%', height: 80, marginBottom: 6 },
                  { width: '100%', height: 80, marginBottom: 6 },
                ]}
                isLoading={product.length == 0}
              > 
            </SkeletonContent>
          </View>
        )
      }
      <ScrollView >
      {
          product.length != 0 && (
            <Layout style={styles.containerLayout} level='1' >
              {
                context.state.product.map((e:any,index:any) => 
                  <ProductCard key={e.id} prod={e} index={index}></ProductCard>
                )
              }
            </Layout>
          )
        }
      </ScrollView>
    </>
  );
}

export const OrderCard = ({navigation}:any) => {
  const context = React.useContext(OrderContext)
  return (
      <>
        {
          context.state.order.length != 0 && (
          <Layout style={styles.containerOverflow} level='1' >
            <Card style={styles.cardOverflow}>
              <View style={{flexDirection:'row',alignItems:'center',backgroundColor:'#F7F7F7'}}>
                
                <View style={{flex:1,backgroundColor:'#F7F7F7'}}>
                  <Text category="h5" style={{textAlign:'left',color: '#01573c'}}>{context.state.order.length} Produk</Text>
                  <View style={{flexDirection:'row',alignItems:'center',marginTop:5,backgroundColor:'#F7F7F7'}}>
                    <Text category="s2" style={{flex:1,textAlign:'left',color: '#01573c'}}>Rp {formatNumber(context.state.total)}</Text>
                  </View>
                </View>
                <View style={{flex:1,backgroundColor:'#F7F7F7'}}>
                <Button style={styles.buyButton} size="medium"  appearance='filled' onPress={() => navigation.navigation.navigate('Pembayaran',{
                  tabBarVisibility: false,
                  section:'toko'
                })}  >
                  Bayar
                </Button>
                </View>
              </View>
            </Card>
          </Layout>
          )
        }
      </>
    );
}

export const OrderLayout = (props:any) => {
  const context = React.useContext(OrderContext)
  return(
    <>
      <Layout style={{...styles.containerLayout,marginBottom:10}} level='1' >
        {
          context.state.order.map((e:any,index:any) => 
          <View style={{flexDirection:'row',width:'100%'}} key={e.id}>
            <View style={styles.ordercard}>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image source={e.image} style={styles.icon}/>
                <View style={{flex:1,backgroundColor:'#fff',marginLeft:10}}>
                  <Text category="s1" style={{textAlign:'left',color: '#01573c',fontWeight:'bold'}}>{e.product_name}</Text>
                  <View style={{flexDirection:'row',alignItems:'center',marginTop:5}}>
                    <Text category="s2" style={{flex:1,textAlign:'left',color: '#01573c'}}>Rp {formatNumber(e.final_price)}</Text>
                  </View>
                </View>
                <View style={{flex:1,backgroundColor:'#fff'}}>
                  <Text category="s1" style={{textAlign:'right',color: '#01573c'}}>x {e.qty}</Text>
                  <View style={{flexDirection:'row',alignItems:'center',marginTop:5}}>
                    <Text category="s2" style={{flex:1,textAlign:'right',color: '#FF9D2B'}}>Rp {formatNumber(e.total_price)}</Text>
                  </View>
                </View>
              </View>
              <View style={{flexDirection:'row',alignItems:'center',marginTop:10}}>
                <Input
                placeholder='Catatan produk'
                  style={styles.input}
                  size="small"
                />
              </View>
            </View>
          </View>
          )
        }
      </Layout>
    </>
  )
}

export const PayCard = ({navigation,slider}:any) => {
  const context = React.useContext(OrderContext)
  return (
      <>
        {
          context.state.order.length != 0 && (
             <View  style={styles.containerPay} >
            {/* <View  style={{flexDirection:'row',alignItems:'center',marginHorizontal:20,marginTop:20}}>
              <Text category="s1" style={{flex:1,textAlign:'left',fontWeight:'bold'}}>Media Pembayaran</Text>
              <Button style={styles.ubahButton} size="small" onPress={() => navigation.navigation.navigate('Pesanan',{
                      tabBarVisibility: false,
                    })} appearance='filled' status='basic' >
                  Cash
              </Button>
            </View> */}
            <Button style={styles.payButton} size="giant"  appearance='filled' onPress={() => slider.current.show()}  >
              Bayar
            </Button>
          </View> 
         
          )
        }
      </>
    );
}

const styles = StyleSheet.create({
  icon:{ 
    width: 80,
    height: 80,
    resizeMode:'contain',
    alignSelf:'center'
  },
  ubahButton:{
    width:150,
    alignSelf:'flex-end',
  },
  categoryButton:{
    alignSelf:'flex-end',
    width:100
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
  },
  containerOverflow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal:10,
    position:'absolute',
    bottom:30,
    backgroundColor:'transparent'
  },
  containerPay: {
    bottom:0,
    marginHorizontal:0,
    height:100,
    width:'100%',
    position:'absolute',
    backgroundColor:'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  payButton:{
    width:'90%',
    backgroundColor:'#01573c',
    borderColor: '#01573c',
    alignSelf:'center',
    marginVertical:10
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal:10
  },
  cardOverflow: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor:'#F7F7F7',
    borderColor:'#F7F7F7',
  },
  input:{
    flex:1,
  },
  ordercard: {
    flex:1,
    marginHorizontal: 13,
    marginTop:20,
    backgroundColor: '#fff',
  },
  card: {
    flex:1,
    marginHorizontal: 10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  totalBadge:{
    backgroundColor:'#fff',
    borderColor: '#fff',
    width:10,
    overflow:'visible',
    borderRadius:5,
    marginHorizontal:5,
    alignSelf:'flex-end',
  },
  plusBadge:{
    height:10,
    overflow:'visible',
    borderRadius:5,
    alignSelf:'flex-end',
  },
  minusBadge:{
    backgroundColor:'#E85656',
    borderColor: '#E85656',
    width:10,
    height:10,
    overflow:'visible',
    borderRadius:5,
    alignSelf:'flex-end',
  },
  buyButton:{
    backgroundColor:'#01573c',
    borderColor: '#01573c',
    alignSelf:'flex-end',
    width:100
  },
  
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    resizeMode:'contain',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:10,
    marginHorizontal:20
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-30,
    backgroundColor: '#fff',
  },
  
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
})