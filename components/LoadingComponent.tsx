import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Card, Modal, Text, Spinner } from '@ui-kitten/components';

export const LoadingComponent = (props:any) => {

  return (
    <Modal
      visible={props.props.visible}
      backdropStyle={styles.backdrop}>
      <Card disabled={true}>
        <View style={{flex:1,alignItems:'center'}}>
          <Spinner status="primary" size='giant' style={{alignSelf:'center'}}/>
          <Text category="p1" style={{marginTop:10}}>Loading, please wait...</Text>
        </View>
      </Card>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    minHeight: 192,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});