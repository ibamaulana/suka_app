import React from 'react';
import { StyleSheet, View,Image } from 'react-native';
import { Layout, Avatar, Icon, MenuItem, OverflowMenu, Text, TopNavigation, TopNavigationAction } from '@ui-kitten/components';
import {UserContext} from '../context/Auth/context';

const MenuIcon = (props?: any) => (
  <Avatar
        style={styles.logo}
        source={require('../assets/images/icon-user.png')}
      />
);

const InfoIcon = (props?: any) => (
  <Icon {...props} name='info'/>
);

const LogoutIcon = (props?: any) => (
  <Icon {...props} name='log-out'/>
);

export const HomeNavigation = (user?:any) => {
  const [menuVisible, setMenuVisible] = React.useState(false);
  const level = (val:any) => {
    if(val == 1){
      return 'Admin';
    }else{
      return 'User';
    }
  }
  const toggleMenu = () => {
    setMenuVisible(!menuVisible);
  };

  const renderMenuAction = () => (
    <TopNavigationAction icon={MenuIcon} />

  );

  const renderTitle = (props?: any) => (
    <View style={styles.titleContainer}>
      <Avatar
        style={styles.logo}
        source={require('../assets/images/logo.png')}
        shape="square"
      />
      <Text category='h5' {...props} style={{color:'#fff'}}>UIN SUKA</Text>
    </View>

  );

  return (
    <TopNavigation
      title={renderTitle}
      accessoryRight={renderMenuAction}
      style={styles.container}
    />
  );
};

export const PageNavigation = (page?:any) => {
  const BackIcon = (props?:any) => (
    <Icon  {...props} name='arrow-back'/>
  );
  
  const BackAction = (props:any) => (
    <TopNavigationAction appearance='control' onPress={() => page.navigation.goBack()} icon={BackIcon}/>
  );

  const renderTitle = (props?: any) => (
    <View style={styles.titleContainer}>
      <Text category='h5' {...props} style={{color:'#fff'}}>{page.title}</Text>
    </View>
  );

  return (
    <TopNavigation
      accessoryLeft={BackAction}
      title={renderTitle}
      style={styles.containerPage}
    />
  );
};

export const PageNavigationLight = (page?:any) => {
  const BackIcon = (props?:any) => (
    <Icon  {...props} name='arrow-back' />
  );
  
  const BackAction = (props:any) => (
    <TopNavigationAction onPress={() => page.navigation.goBack()}  icon={BackIcon}/>
  );

  const renderTitle = (props?: any) => (
    <View style={styles.titleContainer}>
      <Text category='h5' {...props} style={{color:'#000'}}>{page.title}</Text>
    </View>
  );

  return (
    <TopNavigation
      accessoryLeft={BackAction}
      title={renderTitle}
      style={styles.containerPageLight}
    />
  );
};

export const BarNavigation = (page?:any) => {
  const BackIcon = (props?:any) => (
    <Icon  {...props} name='arrow-back'/>
  );
  
  const BackAction = (props:any) => (
    <TopNavigationAction appearance='control' onPress={() => page.navigation.goBack()} icon={BackIcon}/>
  );

  const renderTitle = (props?: any) => (
    <View style={{...styles.titleContainer,paddingTop:20}}>
      <Text category='h5' {...props} style={{color:'#fff'}}>{page.title}</Text>
    </View>
  );

  return (
    <Layout level='1'>
    <TopNavigation
      title={renderTitle}
      alignment="center"
      style={{...styles.containerPage,height:100}}
    />
    </Layout>
  );
};

const styles = StyleSheet.create({
  container:{
    paddingTop: 50,
    paddingBottom: 80,
    backgroundColor: '#01573c',
  },
  containerPage:{
    paddingTop: 50,
    paddingBottom: 20,
    backgroundColor: '#01573c',
    color:'#fff',
    width:'100%',
  },
  containerPageLight:{
    paddingTop: 50,
    paddingBottom: 20,
    backgroundColor: 'white',
    color:'#000',
    width:'100%',
  },
  titleContainerHome: {
    flexDirection: 'row',
    flex:1,
    alignItems: 'flex-start',
    width:'100%'
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logo: {
    marginHorizontal: 16,
  },
  logoimage:{
    flex:1,
    alignSelf:'flex-start',
    resizeMode:'contain',
    width:150,
    height:35,
  }
});