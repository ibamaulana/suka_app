import React from 'react';
import { Image,StyleSheet, View } from 'react-native';
import { Layout } from '@ui-kitten/components';
import Carousel from 'react-native-snap-carousel';

export const ImageCarousel = (props:any) => {
   
    const renderItem = ({item, index}:any) => {
        return (
          <View>
            <Image source={item.image} style={styles.carouselImage} /> 
          </View>
        );
    }

    return (
    <Layout style={styles.carouselLayout} level='1'>
        <Carousel
          layout={"default"}
          data={props.item}
          renderItem={renderItem}
          sliderWidth={330}
          itemWidth={330}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          enableMomentum={false}
          autoplay={true}
          lockScrollWhileSnapping={true}
          loop={true}
          enableSnap={true}
          activeSlideAlignment={'start'}
        />
       </Layout>
    );
}


const styles = StyleSheet.create({
  carouselImage:{ 
    width: 'auto',
    height: 210,
    marginRight:10,
    marginTop:20,
    borderRadius: 10,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: 'transparent'
  },
  carouselLayout: {
    marginLeft:20,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor:'transparent'
  }
});