import React from 'react';
import { StyleSheet} from 'react-native';
import { Input,Button,Select,SelectItem,IndexPath  } from '@ui-kitten/components';
import { View } from '../../components/Themed';
import { percentIcon } from '../../components/Icon';
import Toast from 'react-native-toast-message';
import { ShopContext } from '../../context/Shop/context';
import { env } from '../../config/environtment';
import { SelectForm } from '../SelectForm';

export const FormShop = (props:any) => {
  const shop = React.useContext(ShopContext)

  React.useEffect(() => {
    const shopAsync = async (token:any) =>{
      try {
        let response = await fetch(
          `${env.API_URL}/shop?shop_id=${props.user.state.user.shop_id}`, {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        if(json.status_code != 200){
          Toast.show({
            type: 'error',
            position: 'top',
            text1: 'Error',
            text2: json.message,
            visibilityTime: 4000,
            autoHide: true,
            topOffset: 80,
            bottomOffset: 80,
          });
        }else{
          shop.dispatch({type:'update_shop',data:json.data})
        }
      } catch (error:any) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: error.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    shopAsync(props.user.state.token);
  },[])
  //tax
  const tax_list = [
    {
      label:'Ada',
      value:1,
    },
    {
      label:'Tidak Ada',
      value:2,
    },
  ];

  //tax type
  const taxType_list = [
    {
      label:'Include',
      value:'include',
    },
    {
      label:'Exclude',
      value:'exclude',
    },
  ];

  //service type
  const serviceType_list = [
    {
      label:'Include',
      value:'include',
    },
    {
      label:'Exclude',
      value:'exclude',
    },
  ];

  const submitForm = async () => {
    props.setLoading(true)
    try {
      let response = await fetch(
        'https://backoffice.provey.id/api/v2/shop/update', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+props.user.state.token
          },
          body: JSON.stringify(shop.state)
        }
      );
      let json = await response.json();
      if(json.status_code != 200){
        props.setLoading(false)
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: json.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }else{
        props.setLoading(false)
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Success',
          text2: "Data berhasil di update!",
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
      } catch (error:any) {
        props.setLoading(false)
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: error.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    
  }
   
  return(
    <>
      <View style={styles.inputContainer}>
        <Input
          label='Nama Toko'
          placeholder='Nama Toko'
          value={shop.state.shop_name}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_name',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Email Toko'
          placeholder='Email Toko'
          value={shop.state.shop_email}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_email',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Alamat Toko'
          placeholder='Alamat Toko'
          value={shop.state.shop_address}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_address',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Instagram'
          placeholder='Instagram'
          value={shop.state.shop_instagram}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_instagram',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Facebook'
          placeholder='Facebook'
          value={shop.state.shop_facebook}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_facebook',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <Input
          label='Website'
          placeholder='Website'
          value={shop.state.shop_web}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'shop_web',
              value:nextValue
            })}
          style={styles.input}
        />
      </View>
      <View style={styles.inputContainer}>
        <SelectForm
          value_list={tax_list}
          label="Tax"
          value={shop.state.tax}
          setValue={(value:any) => shop.dispatch(
          {
            type:'update-field',
            field:'tax',
            value:value
          })}
        ></SelectForm>
        <Input
          label='Service'
          placeholder='service'
          value={shop.state.service}
          onChangeText={nextValue => shop.dispatch(
            {
              type:'update-field',
              field:'service',
              value:nextValue
            })}
          accessoryRight={percentIcon}
          style={styles.input}
          keyboardType="numeric"
        />
      </View>
      <View style={styles.inputContainer}>
        <SelectForm
          value_list={taxType_list}
          label="Tipe Tax"
          value={shop.state.tax_type}
          setValue={(value:any) => shop.dispatch(
          {
            type:'update-field',
            field:'tax_type',
            value:value
          })}
        ></SelectForm>
        <SelectForm
          value_list={serviceType_list}
          label="Tipe Service"
          value={shop.state.service_type}
          setValue={(value:any) => shop.dispatch(
          {
            type:'update-field',
            field:'service_type',
            value:value
          })}
        ></SelectForm>
      </View>
      <Button status="primary" onPress={() => submitForm()} style={styles.payButton} size="giant"  appearance='filled'   >
        Simpan
      </Button>
    </>
  )
}


const styles = StyleSheet.create({
  payButton:{
    width:'90%',
    backgroundColor:'#2BCC9C',
    borderColor: '#2BCC9C',
    alignSelf:'center',
    marginVertical:10
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:8,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
    
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  icon:{ 
    width: 50,
    height: 50,
    resizeMode:'contain',
    alignSelf:'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-50,
    marginBottom:20,
    padding:20,
    backgroundColor: '#fff',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },

  menu: {
    marginTop:20,
    backgroundColor: '#ECF5FF',
    borderWidth:0,
    borderRadius:10,
    height:95,
    width:95,
    justifyContent:'center'
  },
});