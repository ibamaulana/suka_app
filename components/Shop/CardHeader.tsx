import React from 'react';
import { StyleSheet } from 'react-native';
import { Button,Text, Layout  } from '@ui-kitten/components';
import { View } from '../../components/Themed';
import { formatNumber, plusIcon } from '../../components/Icon';
import { UserContext } from '../../context/Auth/context';
import Toast from 'react-native-toast-message';
import { ReportContext } from '../../context/Report/context';
import { env } from '../../config/environtment';
import { UsersContext } from '../../context/Users/context';

export const CardHeader = (props:any) => {
  const report = React.useContext(ReportContext)
  const user = React.useContext(UsersContext)
  // React.useEffect(() => {
  //   const dashboardAsync = async (token:any) =>{
  //     try {
  //       let response = await fetch(
  //         `${env.API_URL}/v2/report/dashboard?outlet_id=${user.data.user.outlet_id}`, {
  //           method: 'GET',
  //           headers: {
  //             Accept: 'application/json',
  //             'Content-Type': 'application/json',
  //             Authorization: 'Bearer '+token
  //           },
  //         }
  //       );
  //       let json = await response.json();
  //       if(json.status_code != 200){
  //         Toast.show({
  //           type: 'error',
  //           position: 'top',
  //           text1: 'Error',
  //           text2: json.message,
  //           visibilityTime: 4000,
  //           autoHide: true,
  //           topOffset: 80,
  //           bottomOffset: 80,
  //         });
  //       }else{
  //         report.dispatch({type:'update-dashboard',dashboard:json.data})
  //       }
  //     } catch (error) {
  //       Toast.show({
  //         type: 'error',
  //         position: 'top',
  //         text1: 'Error',
  //         text2: error.message,
  //         visibilityTime: 4000,
  //         autoHide: true,
  //         topOffset: 80,
  //         bottomOffset: 80,
  //       });
  //     }
  //   }
  //   dashboardAsync(user.data.token)
  // },[])
  return(
    <Layout style={styles.containerLayout} level='1'>
      <View style={styles.cardHeaderTop}>
        <View style={{flexDirection:'row',backgroundColor:'#fff',borderBottomColor: '#ECF2FF',borderBottomWidth:2,marginBottom:20}}>
          <Text category="h5" style={styles.shop}>{user.state.shop.shop_name}</Text>
        </View>
        <View style={{flexDirection:'row'}}>
          <View style={{flex:1,backgroundColor:'#fff'}}>
            <Text category="s1" style={styles.headerLeft}>Pemasukan </Text>
            <Text category="h5" style={styles.headerLeft}>{formatNumber(report.state.dashboard.totalgross)}</Text>
          </View>
          <View style={{flex:1,backgroundColor:'#fff'}}>
            <Text category="s1" style={{textAlign:'right',color: '#01573c'}}>Transaksi</Text>
            <Text category="h5" style={{textAlign:'right',color: '#01573c'}}>{report.state.dashboard.totaltransaction}</Text>
          </View>
        </View>
        <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
          <Button appearance='filled'  onPress={() => props.navigation.push('Pesanan',{
          tabBarVisibility: false,
        })} style={styles.button} status='primary' accessoryLeft={plusIcon} >
            Buat Pesanan
          </Button>
        </View>
      </View>
    </Layout>
  )
}

const styles = StyleSheet.create({
  payButton:{
    width:'90%',
    backgroundColor:'#2BCC9C',
    borderColor: '#2BCC9C',
    alignSelf:'center',
    marginVertical:10
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical:8,
    marginHorizontal:20,
  },
  input:{
    flex:1,
    textAlign:'left'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    
    
  },
  shop:{
    flex:1,
    textAlign:'center',
    marginBottom:10
  },
  button: {
    flex:1,
    marginTop:20,
  },
  headerLeft:{
    color: '#01573c'
  },
  logo:{ 
    width: '100%',
    resizeMode:'contain',
    marginTop:20,
  },
  icon:{ 
    width: 50,
    height: 50,
    resizeMode:'contain',
    alignSelf:'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    flex:1
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:'100%'
  },
  containerLayout: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cardHeaderTop: {
    flex:1,
    marginHorizontal: 20,
    marginTop:-50,
    marginBottom:20,
    padding:20,
    backgroundColor: '#fff',
    borderWidth:0,
    borderRadius:10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  cardHeader: {
    flex:1,
    marginHorizontal: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuLeft: {
    flex:1,
    marginLeft: 20,
    marginRight:10,
    marginTop:20,
    backgroundColor: '#fff',
  },
  menuRight: {
    flex:1,
    marginRight: 20,
    marginTop:20,
    backgroundColor: '#fff',
  },

  menu: {
    marginTop:20,
    backgroundColor: '#ECF5FF',
    borderWidth:0,
    borderRadius:10,
    height:95,
    width:95,
    justifyContent:'center'
  },
});