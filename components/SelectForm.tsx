import React from 'react';
import { StyleSheet} from 'react-native';
import { Select, SelectItem, IndexPath, Text } from '@ui-kitten/components';

export const SelectForm = (props:any) => {
  const [index, setIndex] = React.useState<any>(new IndexPath(0));
  const selected = props.value_list[index.row];
  const setValue = (index:any) => {
    setIndex(index);
    props.setValue(props.value_list[index.row].value)
  }

  React.useEffect(() => {
    if(props.value != '' && props.value_list.length != 0){
      let filtered_index = 0;
      filtered_index = props.value_list.findIndex((val:any) => val.value === props.value);
  
      setIndex(new IndexPath(filtered_index))
    }
  },[props.value])

  const renderOption = (data:any) => (
    <SelectItem key={data.value}  title={data.label}/>
  );

  return(
    <Select
      label={() => <Text category='h5' style={{fontSize:17,marginVertical:10}}>{props.label}</Text>}
      style={styles.input}
      value={selected.label}
      selectedIndex={index}
      onSelect={(index:any) => setValue(index)}
      >
      {props.value_list.map(renderOption)}
    </Select>
  )
}

const styles = StyleSheet.create({
  input:{
    flex:1,
    textAlign:'left'
  },
})
