import * as SecureStore from 'expo-secure-store';
import React from 'react';
import Toast from 'react-native-toast-message';
import { ClubContext } from './context';

export function ClubProvider(props?:any){
  const [state,dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'update-field':
          prevState[action.field] = action.value
          return {
            ...prevState
          };
        case 'reset':
          return {
            status_layanan:'umum',
            durasi:'umum',
            total_kamar:1,
            total_tamu:1,
          };
        default:
          throw new Error('unexpected action type');
      }
    },
    {
      status_layanan:'umum',
      durasi:'umum',
      total_kamar:1,
      total_tamu:1,
    }
  );
 
  const value = {
    state, 
    dispatch,
  }
  return (
    <ClubContext.Provider value={value} >
        {props.children}
    </ClubContext.Provider>
  )
}
