import React from 'react';
type Action = {type: 'increment'} | {type: 'decrement'}

export const ClubContext = React.createContext({
  state:{
    status_layanan:'umum',
    durasi:'umum',
    total_kamar:1,
    total_tamu:1,
  },
  dispatch: ({}) => {},
})
export const ClubConsumer = ClubContext.Consumer