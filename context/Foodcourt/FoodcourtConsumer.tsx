import React from 'react';
import { FoodcourtContext } from './context';

export function FoodcourtProvider(props?:any){
  const [state,dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'update_product':
          return {
            ...prevState,
            allproduct: action.product,
            product: action.product,
          };
        case 'update_order':
          return {
            ...prevState,
            order: action.order,
          };
        case 'setLoading':
          return {
            ...prevState,
            isLoading:action.isLoading,
          }
        case 'update_edc':
          return {
            ...prevState,
            edc: action.edc,
          };
        case 'update_ewallet':
          return {
            ...prevState,
            ewallet: action.ewallet,
          };
        case 'update_change':
          return {
            ...prevState,
            change: action.change,
          };
        case 'update_payment':
          return {
            ...prevState,
            total: action.total,
            discount: action.discount,
            pay: action.pay,
            change: action.change,
            payment_type: action.payment_type,
            payment_id: action.payment_id,
            promo_id: action.promo_id,
          };
        case 'remove_order':
          return {
            ...prevState,
            order:action.order
          }
        case 'update_search':
          return {
            ...prevState,
            search:action.search,
            productFiltered:action.productFiltered
          }
        case 'customer_name':
          return {
            ...prevState,
            customer_name:action.value,
          }
        case 'customer_email':
          return {
            ...prevState,
            customer_email:action.value,
          }
        case 'customer_phone':
          return {
            ...prevState,
            customer_phone:action.value,
          }
        case 'searchProduct':
          const dataproduct = prevState.product
          const filtered = dataproduct.filter((p:any) => {return p.product_name.includes(action.value) })
          if(action.value){
            return {
              ...prevState,
              search:action.value,
              product:filtered
            }
          }else{
            return {
              ...prevState,
              search:action.value,
              product:prevState.allproduct
            }
          }
        case 'addProduct':
          if(action.products.stock != 0){
            var produk:any = {};
            const order = prevState.order
            const prod = prevState.product 
            const allprod = prevState.allproduct 
           
            prod.map((e:any,index:any) => {
              if(e.id == action.products.id){
                prod[index] = {
                  ...prod[index],
                  qty: action.qty,
                  stock: action.stock
                }
              }
            })
            allprod.map((e:any,index:any) => {
              if(e.id == action.products.id){
                allprod[index] = {
                  ...allprod[index],
                  qty: action.qty,
                  stock: action.stock
                }
              }
            })
            const filtered = order.filter((e:any) => {
              if(action.products.id == e.id){
                e.qty = action.qty
                e.total_price = e.final_price*e.qty
                return true
              }
            })
            if(filtered.length == 0){
              produk = { 
                id: action.products.id,
                image:action.products.image,
                product_name: action.products.product_name,
                categories: action.products.categories,
                base_price: action.products.base_price,
                final_price: action.products.final_price,
                discount_price: action.products.discount_price,
                total_price: action.products.final_price,
                qty: 1,
              }
              order.push(produk)
            }
            prevState.total = action.total
            prevState.pay = action.pay

            return {
              ...prevState,
              order:order,
            } 
          }else{
            return {
              ...prevState
            }
          }
        case 'removeProduct':
            var produk:any = {};
            const order = prevState.order
            const prod = prevState.product 
            const allprod = prevState.allproduct 
            prod.map((e:any,index:any) => {
              if(e.id == action.products.id){
                prods[index] = {
                  ...prods[index],
                  qty: action.qty,
                  stock: action.stock
                }
              }
            })
            allprod.map((e:any,index:any) => {
              if(e.id == action.products.id){
                allprod[index] = {
                  ...allprod[index],
                  qty: action.qty,
                  stock: action.stock
                }
              }
            })
            const updateorder:any = []
            order.map(function(e:any){
              if(action.products.id == e.id){
                e.qty = action.qty
                e.total_price = action.total
              }
              if(e.qty != 0){
                updateorder.push(e)
              }
            })
            prevState.total = action.total
            prevState.pay = action.pay
            return {
              ...prevState,
              order:updateorder,
            }
        case 'payment':
          const prods = prevState.product 
          const allprods= prevState.allproduct 
          
          prods.map((e:any,index:any) => {
            prods[index] = {
              ...prods[index],
              qty: 0,
            }
          })
          allprods.map((e:any,index:any) => {
            allprods[index] = {
              ...allprods[index],
              qty: 0,
            }
          })
          return {
            ...prevState,
            order:[],
            edc:[],
            ewallet:[],
            promo:[],
            search:'',
            total: 0,
            discount: 0,
            pay: 0,
            change: 0,
            payment_type: 'cash',
            payment_id: 0,
            promo_id:0
          };
        default:
          throw new Error('unexpected action type');
      }
    },
    {
      allproduct:[],
      product:[],
      productFiltered:[],
      order:[],
      edc:[],
      ewallet:[],
      promo:[],
      search:'',
      total: 0,
      discount: 0,
      pay: 0,
      change: 0,
      payment_type: 'cash',
      payment_id: 0,
      promo_id:0,
      customer_name:'',
      customer_email:'',
      customer_phone:'',
      isLoading: false
    }
  );
 
  const value = {
    state, 
    dispatch,
  }
  return (
    <FoodcourtContext.Provider value={value} >
        {props.children}
    </FoodcourtContext.Provider>
  )
}
