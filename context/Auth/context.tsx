import React from 'react';
export const UserContext = React.createContext({
    checkToken: async () => {},
    signUp: async (data:any) => {},
    signIn: async (data:any) => {},
    signOut: async  () => {},
    data: {
      user:{
        name: '',
        shop_id: null,
        outlet_id:null,
        role:{
          name:''
        }
      },
      shop:{
        shop_name: ''
      },
      outlet:{
        name: ''
      },
      token:null,
    }
  })
  export const UserConsumer = UserContext.Consumer