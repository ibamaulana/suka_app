import React from 'react';
import Toast from 'react-native-toast-message';
import * as SecureStore from 'expo-secure-store';
import { UserContext } from './context';

export function UserProvider(props?:any){
  const [state, dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'SIGN_IN':
          return {
            ...prevState,
            token: action.token,
            user: action.user,
            shop: action.user.shop,
            outlet: action.user.outlet
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            token: null,
          };
        case 'SET_USER':
          return {
            ...prevState,
            user:action.user,
            shop:action.shop,
            outlet:action.outlet
          }
        case 'SET_TOKEN':
          return {
            ...prevState,
            token:action.token
          }
      }
    },
    {
      token: null,
      user:{
        name: '',
        role:{
          name:''
        }
      },
      shop:{
        shop_name: ''
      },
      outlet:{
        name: ''
      }
    }
  );
  React.useEffect(() => {
    console.log('user context')
    const tokenAsync = async () => {
      let userToken;
      try {
        userToken = await SecureStore.getItemAsync('userToken');
        if(userToken != null){
          userAsync(userToken)
          dispatch({
            type: 'SET_TOKEN',
            token:userToken,
          })
        }
      } catch (e) {
        
      }
    };
    const userAsync = async (token?: any) =>{
      try {
        let response = await fetch(
          'https://backoffice.provey.id/api/user/detail', {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        if(json.status_code != 200){
          Toast.show({
            type: 'error',
            position: 'top',
            text1: 'Error',
            text2: 'yang ini',
            visibilityTime: 4000,
            autoHide: true,
            topOffset: 80,
            bottomOffset: 80,
          });
        }else{
          dispatch({
            type: 'SET_USER',
            user:json.data,
            shop:json.data.shop,
            outlet:json.data.outlet
          })
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: 'yang itu',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    tokenAsync()
  },[])

  React.useEffect(() => {
    const tokenAsync = async () => {
      let userToken;
      try {
        userToken = await SecureStore.getItemAsync('userToken');
        if(userToken != null){
          userAsync(userToken)
          dispatch({
            type: 'SET_TOKEN',
            token:userToken,
          })
        }
      } catch (e) {
        
      }
    };
    const userAsync = async (token?: any) =>{
      try {
        let response = await fetch(
          'https://backoffice.provey.id/api/user/detail', {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        if(json.status_code != 200){
          Toast.show({
            type: 'error',
            position: 'top',
            text1: 'Error',
            text2: 'yang ini',
            visibilityTime: 4000,
            autoHide: true,
            topOffset: 80,
            bottomOffset: 80,
          });
        }else{
          dispatch({
            type: 'SET_USER',
            user:json.data,
            shop:json.data.shop,
            outlet:json.data.outlet
          })
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: 'yang itu',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    tokenAsync()
  },[state.token])

  const checkToken = async () =>{
    let userToken;
    try {
      userToken = await SecureStore.getItemAsync('userToken');
    } catch (e) {
    }
    dispatch({ type: 'SET_TOKEN', token: userToken });
    console.log(state)

  }

  const signIn = async (data?:any) => {
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/login', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: data.email,
            password: data.password
          })
        }
      );
      let json = await response.json();
      if(json.status_code != 200){
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: json.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }else{
        dispatch({ type: 'SIGN_IN', token: json.token, user: json.data });
        try {
          await SecureStore.setItemAsync('userToken',json.token);
        } catch (e) {
          console.log(e)
        }
      }
    } catch (error:any) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: error.message,
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  const signUp = async (data?:any) => {
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/register', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            email: data.email,
            password: data.password
          })
        }
      );
      let json = await response.json();
      if(json.status_code != 200){
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: json.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }else{
        dispatch({ type: 'SIGN_IN', token: json.token, user: json.data });
        try {
          await SecureStore.setItemAsync('userToken',json.token);
        } catch (e) {
          console.log(e)
        }
      }
    } catch (error:any) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: error.message,
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  const signOut =  async () => {
    console.log('logout')
    try {
      await SecureStore.deleteItemAsync('userToken');
      console.log('logout')
      dispatch({ type: 'SIGN_OUT' })
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <UserContext.Provider value={{
      checkToken: checkToken,
      signUp: signUp,
      signIn: signIn,
      signOut: signOut,
      data: state
    }} >
        {props.children}
    </UserContext.Provider>
  )
}
