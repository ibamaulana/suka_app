import * as SecureStore from 'expo-secure-store';
import React, { useContext } from 'react';
import Toast from 'react-native-toast-message';
import { env } from '../../config/environtment';
import { UsersContext } from '../Users/context';
import { DashboardContext } from './context';

export function DashboardProvider(props?:any){
  const [state,dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'update-field':
          prevState[action.field] = action.value
          return {
            ...prevState
          };
        default:
          throw new Error('unexpected action type');
      }
    },
    {
      tahun: 2021,
      bulan: 11,
      data:{
        pengadaan: {
          proses: 0,
          diterima: 0,
          ditolak:0
        },
        pengadaan_pp: {
          proses: 0,
          diterima: 0,
          ditolak:0
        },
        permintaan: {
          proses: 0,
          diterima: 0,
          ditolak:0
        }
      }
      
    }
  );
  
  const getData = async () => {
    try {
      let response = await fetch(
        `${env.API_URL}/dashboard?tahun=${state.tahun}&bulan=${state.bulan}`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            '_token': props.user.state.token
          },
        }
      );
      let json = await response.json();
      console.log(json)
      dispatch({type:'update-field',field:'data',value:json.data})
    } catch (error:any) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: 'error',
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  React.useEffect(() => {
    getData()
  },[state.tahun,state.bulan])
 
  const value = {
    state, 
    dispatch
  }
  return (
    <DashboardContext.Provider value={value} >
        {props.children}
    </DashboardContext.Provider>
  )
}
