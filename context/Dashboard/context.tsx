import React from 'react';
type Action = {type: 'increment'} | {type: 'decrement'}

export const DashboardContext = React.createContext({
  state:{
    tahun: 2021,
    bulan: 11,
    data:{
      pengadaan: {
        proses: 0,
        diterima: 0,
        ditolak:0
      },
      pengadaan_pp: {
        proses: 0,
        diterima: 0,
        ditolak:0
      },
      permintaan: {
        proses: 0,
        diterima: 0,
        ditolak:0
      }
    }
  },
  dispatch: ({}) => {},
})
export const DashboardConsumer = DashboardContext.Consumer