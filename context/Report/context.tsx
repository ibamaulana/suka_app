import React from 'react';
type Action = {type: 'increment'} | {type: 'decrement'}
type Dispatch = (action: Action) => void
type State = {product: any}
export const ReportContext = React.createContext({
  state:{
    dashboard:{
      totaltransaction:0,
      totalnet:0,
      totalgross:0
    }
  },
  dispatch: ({}) => {},
})
export const ReportConsumer = ReportContext.Consumer