import React from 'react';
import { ReportContext } from './context';

export function ReportProvider(props?:any){
  const [state,dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'update-dashboard':
          return {
            ...prevState,
            dashboard: action.dashboard,
          };
        default:
          throw new Error('unexpected action type');
      }
    },
    {
      dashboard:{
        totaltransaction:0,
        totalnet:0,
        totalgross:0
      }
    }
  );
 
  const value = {
    state, 
    dispatch,
  }
  return (
    <ReportContext.Provider value={value} >
        {props.children}
    </ReportContext.Provider>
  )
}
