import React from 'react';
type Action = {type: 'increment'} | {type: 'decrement'}
type Dispatch = (action: Action) => void
type State = {product: any}
export const OrderContext = React.createContext({
  state:{
    allproduct:[],
    product:[],
    productFiltered:[],
    order:[],
    edc:[],
    ewallet:[],
    promo:[],
    search:'',
    total: 0,
    discount: 0,
    pay: 0,
    change: 0,
    payment_type: 'cash',
    payment_id: 0,
    promo_id:0,
    customer_name:'',
    customer_email:'',
    customer_phone:'',
    isLoading: false
  },
  dispatch: ({}) => {},
})
export const OrderConsumer = OrderContext.Consumer