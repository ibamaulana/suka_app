import React from 'react';
type Action = {type: 'increment'} | {type: 'decrement'}

export const FutsalContext = React.createContext({
  state:{
    order_type: 'futsal',
    layanan_index: 0,
    layanan: 'reguler',
    status_layanan:'umum',
    date: new Date(),
    range_date: {startDate: new Date(), endDate: new Date()}
  },
  dispatch: ({}) => {},
})
export const FutsalConsumer = FutsalContext.Consumer