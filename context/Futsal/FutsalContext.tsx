import * as SecureStore from 'expo-secure-store';
import React from 'react';
import Toast from 'react-native-toast-message';
import { FutsalContext } from './context';

export function FutsalProvider(props?:any){
  const [state,dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'update-field':
          prevState[action.field] = action.value
          return {
            ...prevState
          };
        case 'reset-futsal':
          return {
            ...prevState,
            order_type: 'futsal',
            status_layanan:'umum',
            date: new Date(),
            range_date: {}
          };
        case 'reset-tenis':
          return {
            ...prevState,
            order_type: 'tenis',
            status_layanan:'umum',
            date: new Date(),
            range_date: {}
          };
        default:
          throw new Error('unexpected action type');
      }
    },
    {
      order_type: 'futsal',
      layanan_index: 0,
      layanan: 'reguler',
      status_layanan:'umum',
      date: new Date(),
      range_date: {startDate: new Date(), endDate: new Date()}
    }
  );
 
  const value = {
    state, 
    dispatch,
  }
  return (
    <FutsalContext.Provider value={value} >
        {props.children}
    </FutsalContext.Provider>
  )
}
