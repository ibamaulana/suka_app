import * as SecureStore from 'expo-secure-store';
import React from 'react';
import Toast from 'react-native-toast-message';
import { UsersContext } from './context';

export function UsersProvider(props?:any){
  const [state,dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'SIGN_IN':
          return {
            ...prevState,
            token: action.token,
            user: action.user,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            token: null,
          };
        case 'SET_USER':
          return {
            ...prevState,
            user:action.user,
          }
        case 'SET_TOKEN':
          return {
            ...prevState,
            token:action.token
          }
        default:
          throw new Error('unexpected action type');
      }
    },
    {
      token: null,
      user:{
        name: '',
      },
    }
  );

  React.useEffect(() => {
    const tokenAsync = async () => {
      let userToken;
      try {
        userToken = await SecureStore.getItemAsync('userToken');
        if(userToken != null){
          userAsync(userToken)
          dispatch({
            type: 'SET_TOKEN',
            token:userToken,
          })
        }
      } catch (e) {
        
      }
    };
    const userAsync = async (token?: any) =>{
      try {
        let response = await fetch(
          'https://uin.vantura.id/api/current', {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+token
            },
          }
        );
        let json = await response.json();
        dispatch({
          type: 'SET_USER',
          user:json.user,
        })
      } catch (error) {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: 'yang itu',
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
    }
    tokenAsync()
  },[])

  const register = async (param:any) => {
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/register', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(param)
        }
      );
      let json = await response.json();
      if(!json.status){
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: json.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }
      
    } catch (error:any) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: error.message,
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  const login = async (param:any) => {
    try {
      let response = await fetch(
        'https://uin.vantura.id/api/login', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username: param.email,
            password: param.password
          })
        }
      );
      let json = await response.json();
      if(!json.success){
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: json.message,
          visibilityTime: 4000,
          autoHide: true,
          topOffset: 80,
          bottomOffset: 80,
        });
      }else{
        dispatch({ type: 'SIGN_IN', token: json.token, user: json.user });
        try {
          await SecureStore.setItemAsync('userToken',json.token);
        } catch (e) {
          console.log(e)
        }
      }
    } catch (error:any) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Error',
        text2: error.message,
        visibilityTime: 4000,
        autoHide: true,
        topOffset: 80,
        bottomOffset: 80,
      });
    }
  }

  const logout = async () => {
    try {
      await SecureStore.deleteItemAsync('userToken');
      console.log('logout')
      dispatch({ type: 'SIGN_OUT' })
    } catch (e) {
      console.log(e)
    }
  }
 
  const value = {
    state, 
    dispatch,
    register,
    login,
    logout
  }
  return (
    <UsersContext.Provider value={value} >
        {props.children}
    </UsersContext.Provider>
  )
}
