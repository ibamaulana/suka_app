import React from 'react';
type Action = {type: 'increment'} | {type: 'decrement'}

export const UsersContext = React.createContext({
  state:{
    token: null,
    user:{
      nama: '',
    },
  },
  dispatch: ({}) => {},
  register: async (param:any) => {},
  login: async (param:any) => {},
  logout: () => {}
})
export const UsersConsumer = UsersContext.Consumer