import React from 'react';
type Action = {type: 'increment'} | {type: 'decrement'}
type Dispatch = (action: Action) => void
type State = {product: any}
export const ShopContext = React.createContext({
  state:{
    shop_name:'',
    shop_address:'',
    shop_contact:'',
    shop_email:'',
    shop_logo:'',
    shop_description:'',
    shop_instagram:'',
    shop_facebook:'',
    shop_web:'',
    tax:'',
    service:'',
    tax_type:'',
    service_type: ''
  },
  dispatch: ({}) => {},
})
export const ReportConsumer = ShopContext.Consumer