import React from 'react';
import { ShopContext } from './context';

export function ShopProvider(props?:any){
  const [state,dispatch] = React.useReducer(
    (prevState:any, action:any) => {
      switch (action.type) {
        case 'update-field':
          prevState[action.field] = action.value
          return {
            ...prevState
          };
        case 'update_shop':
          return{
            shop_name:action.data.shop_name,
            shop_address:action.data.shop_address,
            shop_contact:action.data.shop_contact,
            shop_email:action.data.shop_email,
            shop_logo:action.data.shop_logo,
            shop_description:action.data.shop_description,
            shop_instagram:action.data.shop_instagram,
            shop_facebook:action.data.shop_facebook,
            shop_web:action.data.shop_web,
            tax:action.data.tax,
            service:action.data.service.toString(),
            tax_type:action.data.tax_type,
            service_type:action.data.service_type
          }
        default:
          throw new Error('unexpected action type');
      }
    },
    {
      shop_name:'',
      shop_address:'',
      shop_contact:'',
      shop_email:'',
      shop_logo:'',
      shop_description:'',
      shop_instagram:'',
      shop_facebook:'',
      shop_web:'',
      tax:'',
      service:'',
      tax_type:'',
      service_type:''
    }
  );
 
  const value = {
    state, 
    dispatch,
  }
  return (
    <ShopContext.Provider value={value} >
        {props.children}
    </ShopContext.Provider>
  )
}
